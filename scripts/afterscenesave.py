import hou
from python import spc_initialize_scene
from python import spc_render_asset_utils
import importlib
importlib.reload(spc_initialize_scene)
importlib.reload(spc_render_asset_utils)

# UPDATE GLOBAL VARIABLES FOR OPENED SCENE FILE
file_path = hou.hipFile.path()
spc_initialize_scene.set_variables(file_path)
hou.hscript('varchange')



###############      RENDER ASSETS CHECK
# CHECK FOR FOREIGN RENDER ASSETS. FOR EXAMPLE IF THIS FILE WAS SAVED TO ANOTHER CONTEXT
foreign_render_assets = spc_render_asset_utils.get_foreign_render_assets()

if foreign_render_assets:
    hda_nodes_string = foreign_render_assets[0].name()
    if len(foreign_render_assets) > 1:
        for index, i in enumerate(foreign_render_assets):
            if index > 0:
                hda_nodes_string.append('\n' + i.name())
    hou.ui.displayMessage('1 or more foreign Render Assets found! Be careful!', title='Warning',
                          severity=hou.severityType.Warning, details='NODES: {}'.format(hda_nodes_string), details_expanded=True)
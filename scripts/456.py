import hou
import os
import sys
import importlib

# APPEND SPACEPIPE PYTHON SCRIPTS TO PYTHONPATH
spacepipe_root = os.environ.get('SPACEPIPE_ROOT')
spacepipe_scripts_dir = os.path.join(spacepipe_root, 'scripts')
sys.path.append(spacepipe_scripts_dir)
from python import spc_initialize_scene
importlib.reload(spc_initialize_scene)


# INITIALIZE GLOBAL VARIABLES FOR OPENED SCENE FILE
file_path = hou.hipFile.path()
spc_initialize_scene.set_variables(file_path)

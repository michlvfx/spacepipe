from PySide2.QtGui import *
from PySide2.QtCore import *
from PySide2.QtWidgets import *


class SpacepipeWindow(QDialog):
    def __init__(self, parent=None):
        super(SpacepipeWindow, self).__init__(parent)
        # MAINWIN SETTINGS
        self.setWindowTitle('SPACEPIPE WINDOW')
        self.resize(350, 455)
        self.setAttribute(Qt.WA_DeleteOnClose)
        # TOP LAYOUT
        self.top_layout = QVBoxLayout()
        self.top_layout.setSpacing(2)
        self.setLayout(self.top_layout)
        # MAIN LAYOUT
        self.main_group = QGroupBox()
        self.main_group.setContentsMargins(-8, -8, -8, -8)
        self.main_layout = QVBoxLayout()
        self.main_group.setLayout(self.main_layout)
        self.top_layout.addWidget(self.main_group)
        # EXECUTION BUTTONS
        self.exec_buttons_hori = QHBoxLayout()
        self.exec_buttons_hori.addStretch()
        self.button_ok = QPushButton('OK')
        self.button_cancel = QPushButton('Cancel')
        self.button_cancel.clicked.connect(self.cancel)
        self.exec_buttons_hori.addWidget(self.button_ok)
        self.exec_buttons_hori.addWidget(self.button_cancel)
        self.top_layout.addLayout(self.exec_buttons_hori)
        # THIS WINDOW ALWAYS STAYS ON TOP
        self.setWindowFlags(Qt.WindowStaysOnTopHint)

    # CANCEL BUTTON CLICKED
    def cancel(self):
        self.close()

import nodesearch
import hou
import spc_cache_utils
import importlib
importlib.reload(spc_cache_utils)


hda_node = kwargs.get('node')

# THIS CONDITION STILL RETURNS TRUE WHEN THE NODE GOT DUPLICATED
if not hou.hipFile.isLoadingHipFile():
    # CHECK IF THE ischild PARM IS TRUE
    # IF YES, ONLY DUPLICATE THE NODE, THE RELATIVE REFERENCES WILL REMAIN THE SAME
    is_child = hda_node.parm('ischild').eval()

    try:
        if not is_child:
            # SET NODE TO READ ONLY
            # UNLOCK FIRST
            hda_node.parm('read_only').lock(False)
            hda_node.setParms({'read_only': True})

            # LOCK READ ONLY PARM
            hda_node.parm('read_only').lock(True)

            # DISCONNECT INPUTS
            hda_node.setInput(0, None, 0)
            hda_node.setInput(1, None, 0)
            hda_node.setInput(2, None, 0)
            hda_node.setInput(3, None, 0)

            # UPDATE COLORS
            spc_cache_utils.read_cache_toggled(hda_node)

            # GET SOURCE HELPER DATA
            source_context = hda_node.parm('source_context').eval()
            source_name = hda_node.parm('source_name').eval()

            # SEARCH THE SOURCE NODE IN GIVEN CONTEXT WITH GIVEN NAME
            matcher = nodesearch.NodeType('space_cacher')
            network = hou.node(source_context)
            source_node = ''
            for node in matcher.nodes(network):
                if node.parm('name').eval() == source_name and not node.parm('read_only').isLocked():
                    source_node = node

            if source_node:
                # CONNECT TO THE PARMS OF THE SOURCE NODE
                source_node_name_parm = source_node.parm('name')
                source_node_file_parm = source_node.parm('file')
                source_node_user_wedge_parm = source_node.parm('user_wedge')
                source_node_type_parm = source_node.parm('type')
                source_node_source_name_parm = source_node.parm('source_name')
                source_node_source_context_parm = source_node.parm('source_context')
                source_node_cacher_parm = source_node.parm('source_cacher_node')

                # UNLOCK PARMS
                hda_node.parm('file').lock(False)
                hda_node.parm('name').lock(False)
                hda_node.parm('user_wedge').lock(False)
                hda_node.parm('type').lock(False)
                hda_node.parm('source_name').lock(False)
                hda_node.parm('source_context').lock(False)
                hda_node.parm('source_cacher_node').lock(False)
                hda_node.parm('ischild').lock(False)

                # SET AS ABSOLUTE PATHS TO ASSURE THE CONNECTION WHEREVER THE NODE IS COPIED TO
                hda_node.parm('file').set('`chsop("' + source_node_file_parm.path() + '")`')
                hda_node.parm('name').set('`chsop("' + source_node_source_name_parm.path() + '")`')
                hda_node.parm('user_wedge').setExpression('ch("' + source_node_user_wedge_parm.path() + '")')
                hda_node.parm('type').setExpression('ch("' + source_node_type_parm.path() + '")')
                hda_node.parm('source_name').set('`chsop("' + source_node_source_name_parm.path() + '")`')
                hda_node.parm('source_context').set('`chsop("' + source_node_source_context_parm.path() + '")`')
                hda_node.parm('source_cacher_node').set('`chsop("' + source_node_cacher_parm.path() + '")`')
                hda_node.parm('ischild').set(True)

                # LOCK PARMS AGAIN
                hda_node.parm('file').lock(True)
                hda_node.parm('name').lock(True)
                hda_node.parm('user_wedge').lock(True)
                hda_node.parm('type').lock(True)
                hda_node.parm('source_name').lock(True)
                hda_node.parm('source_context').lock(True)
                hda_node.parm('source_cacher_node').lock(True)
                hda_node.parm('filemode').lock(True)
                hda_node.parm('ischild').lock(True)

        # IF CHILD NODE, CONFORM INPUT / OUTPUT COLORS
        else:
            # UPDATE COLORS
            spc_cache_utils.update_input_output_colors(hda_node)
    except:
        pass

import hou
import os
import nodesearch
import spc_render_asset_utils
import spc_structurize
import importlib
importlib.reload(spc_render_asset_utils)
importlib.reload(spc_structurize)


# PUBLISH
def publish_render_asset():
    # SAVE THE HIP FILE FIRST TO ASSURE THAT THE HDA WILL BE REPRODUCABLE WITH THE HIPFILE
    hou.hipFile.save()

    # GET INFO FOR NEW RENDER ASSET ACCORDING TO CURRENT HIPFILE
    hda_info_dict = spc_render_asset_utils.create_hda_info_from_hip()
    target_hda_name = hda_info_dict.get('hda_name')
    render_asset_node = None

    # FIRST CHECK FOR A READY TO BE EXPORTED ASSET WHICH CORRESPONDS TO THIS HIPFILE VERSION
    current_render_asset_node = spc_render_asset_utils.get_current_render_asset_from_hip()
    if current_render_asset_node:
        render_asset_node = current_render_asset_node
        is_current_version = True
    else:
        other_render_asset_nodes = spc_render_asset_utils.get_render_assets_to_be_exported()
        if other_render_asset_nodes:
            if len(other_render_asset_nodes) > 1:
                # MORE THAN 1 ASSET FOUND THAT HAS THE 'to_be_exported' FLAG?
                choices = [i.name() for i in other_render_asset_nodes]
                user_choice = hou.ui.selectFromList(choices, exclusive=True, message='Choose desired Render Asset to be published.',
                                                    title='Publish Render Asset', clear_on_cancel=True, column_header='Render Assets', height=150)
                if user_choice:
                    render_asset_node = other_render_asset_nodes[user_choice[0]]
            else:
                render_asset_node = other_render_asset_nodes[0]
        is_current_version = False

    # RENDER ASSET HDA TO BE EXPORTED FOUND
    if render_asset_node:
        # CHECK IF HDA IS ALREADY LOCKED
        locked = render_asset_node.isLockedHDA()
        if not locked:

            ######## !!! IF HDA IS CURRENT HIP FILE VERSION, THEN JUST UPDATE THE HDA
            ######## !!! IF IT'S NOT THE CURRENT HIP FILE VERSION (eg HIP FILE v005 BUT HDA STILL v002) THEN WE HAVE TO CREATE A NEW DEFINITION

            # HDA ALREADY MATCHES CURRENT HIP FILE VERSION
            if is_current_version:
                # FULL HDA FILE PATH
                hda_full_path = spc_render_asset_utils.get_filepath_from_hda(render_asset_node)
                # EXISTENCE CHECK
                if overwrite_if_existing(hda_full_path):
                    # UPDATE TO PUBLISHED STATE
                    spc_render_asset_utils.update_render_asset_appearance(render_asset_node, hda_full_path, 'published')
                    # SAVE CURRENT STATE TO HDA AND LOCK IT
                    render_asset_node.type().definition().updateFromNode(render_asset_node)
                    render_asset_node.matchCurrentDefinition()
                    # SET DEFINITION STATUS TO PUBLISHED
                    spc_render_asset_utils.change_hda_definition_status('published', hda_file_path=hda_full_path)
                    # SET FOCUS TO PUBLISHED HDA NODE
                    spc_render_asset_utils.set_focus_to_hda_node(render_asset_node)

            # HDA VERSION DOES NOT MATCH CURRENT HIP FILE VERSION
            else:
                # GET ALL THE NECESSARY PRE CONSTRUCTED DATA FOR THE HDA
                hda_name = hda_info_dict.get('hda_name')
                hda_full_path = hda_info_dict.get('hda_full_path')
                hda_node_name = hda_info_dict.get('hda_node_name')
                hda_version = hda_info_dict.get('hda_version')

                # EXISTENCE CHECK
                if overwrite_if_existing(hda_full_path):
                    hda_definition = render_asset_node.type().definition()
                    # COPY THE DEFINITION AND CREATE A NEW HDA FILE
                    hda_definition.copyToHDAFile(hda_full_path, new_name=hda_name, new_menu_name=hda_node_name)
                    hou.hda.installFile(hda_full_path)
                    new_render_asset_node = render_asset_node.changeNodeType(hda_name, keep_name=False, keep_parms=True, keep_network_contents=True)

                    new_definition = new_render_asset_node.type().definition()
                    new_definition.setVersion(hda_version)
                    new_render_asset_node.type().definition().updateFromNode(new_render_asset_node)
                    spc_render_asset_utils.update_render_asset_appearance(new_render_asset_node, hda_full_path, 'published')
                    new_render_asset_node.matchCurrentDefinition()
                    # CONFORM NAME TO NEW HDA VERSION
                    new_render_asset_node.setName(hda_node_name)
                    # SET DEFINITION STATUS TO PUBLISHED
                    spc_render_asset_utils.change_hda_definition_status('published', hda_file_path=hda_full_path)
                    # SET FOCUS TO PUBLISHED HDA NODE
                    spc_render_asset_utils.set_focus_to_hda_node(new_render_asset_node)

        # HDA IS ALREADY LOCKED
        else:
            hou.ui.displayMessage('HDA is already locked!', title='Error',
                                  severity=hou.severityType.Error, details='HDA: {}'.format(target_hda_name), details_expanded=True)
    # NO RENDER ASSET HDA FOUND
    else:
        hou.ui.displayMessage('Could not find Render Asset which corresponds to this version!', title='Error',
                          severity=hou.severityType.Error, details='Searched for: {}'.format(target_hda_name), details_expanded=True)



# CHECK FOR EXISTENCE
def overwrite_if_existing(hda_file_path):
    keep_going = False
    if os.path.isfile(hda_file_path):
        # CHECK IF THE EXISTING ONE IS PUBLISHED
        status = spc_render_asset_utils.get_render_asset_status(hda_file_path=hda_file_path)
        if status == 'PUBLISHED' or status == 'published':
            choice = hou.ui.displayMessage('Digital Asset already exists!', title='Warning', buttons=('Overwrite', 'Cancel'),
                              severity=hou.severityType.Warning, details=hda_file_path, details_expanded=True)
            if choice == 0:
                keep_going = True
            elif choice == 1:
                keep_going = False
        else:
            keep_going = True
    else:
        keep_going = True

    return keep_going

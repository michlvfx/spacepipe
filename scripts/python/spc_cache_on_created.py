import spc_cache_utils
import importlib
importlib.reload(spc_cache_utils)

hda_node = kwargs.get('node')

# INIT COLOR
spc_cache_utils.set_hda_node_color(hda_node)

# SET FILE PATH
cache_path = spc_cache_utils.create_cache_path(kwargs)
hda_node.setParms({'file': cache_path})
file_parm = hda_node.parm('file')
file_parm.lock(True)

# SET SOURCE CONTEXT HELPER PARM
source_helper_string_parm = hda_node.parm('source_context')
source_helper_string_parm.set(hda_node.path().rsplit('/', 1)[0])

# SET SOURCE NODE PARM
source_cacher_node_parm = hda_node.parm('source_cacher_node')
source_cacher_node_parm.set('.')

# SHOW NODE COMMENT IN NETWORK VIEW
hda_node.setGenericFlag(hou.nodeFlag.DisplayComment, True)

# SET FILECACHE MODE TO NO OPERATION
hda_node.setParms({'filemode': 3})

# SET INPUT AND OUTPUT COLORS
spc_cache_utils.activate_inputs(hda_node, activate=False)
spc_cache_utils.activate_outputs(hda_node, activate=False)

# SET INITIAL COMMENT
hda_node.setComment('Inactive')

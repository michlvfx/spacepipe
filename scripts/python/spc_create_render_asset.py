import hou
import os
import nodesearch
import spc_render_asset_utils
import spc_structurize
import importlib
importlib.reload(spc_render_asset_utils)
importlib.reload(spc_structurize)

def create_render_asset():
    # GET THE RENDER ASSET SUBNETWORK
    render_asset_subnet = spc_render_asset_utils.get_render_asset_subnet()

    if render_asset_subnet:
        # SET THE "render_asset" PARM TO "to_be_exported"
        render_asset_subnet.parm('render_asset').set('to_be_exported')
        # GET INFO FOR NEW RENDER ASSET ACCORDING TO CURRENT HIPFILE
        hda_info_dict = spc_render_asset_utils.create_hda_info_from_hip()
        hda_name = hda_info_dict.get('hda_name')
        hda_full_path = hda_info_dict.get('hda_full_path')
        hda_node_name = hda_info_dict.get('hda_node_name')
        hda_version = hda_info_dict.get('hda_version')

        hda_node = render_asset_subnet.createDigitalAsset(name=hda_name, hda_file_name=hda_full_path, description=hda_node_name, version=hda_version,
                                                  ignore_external_references=True)
        # SET HDA NODE NAME
        hda_node.setName(hda_node_name)

        # SET NODE SHAPE
        hda_node.setUserData('nodeshape', 'cigar')

        # UPDATE HDA APPEARANCE
        hda_state = 'unpublished'
        spc_render_asset_utils.update_render_asset_appearance(hda_node, hda_full_path, hda_state)

        # TAG THE USER INFO SO OTHER SCRIPTS CAN SEE THAT THIS IS A PIPELINE RENDER ASSET HDA
        hda_definition = spc_render_asset_utils.get_definition_from_hda_file(hda_full_path)
        hda_definition.setUserInfo('spacepipe_render_asset')


        # SET FOCUS ON NEWLY CREATED RENDER ASSET
        spc_render_asset_utils.set_focus_to_hda_node(hda_node)


    # NO RENDER ASSET SUBNET FOUND
    else:
        hou.ui.displayMessage('Could not find Render Asset Subnetwork!', title='Error',
                              severity=hou.severityType.Error)


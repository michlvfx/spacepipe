from PySide2.QtGui import *
from PySide2.QtCore import *
from PySide2.QtWidgets import *
import hou
import os
import collections
import spc_pipeline_win as spcwin
import spc_render_asset_utils
import spc_structurize
import spc_utils
import spc_global_pipeline_vars as spcvars
import importlib
importlib.reload(spcwin)
importlib.reload(spc_render_asset_utils)
importlib.reload(spc_structurize)
importlib.reload(spc_utils)
importlib.reload(spcvars)
from pathlib import Path

# GLOBAL VARS
pipeline_window = spcwin.SpacepipeWindow  # BASIC PIPELINE WINDOW CLASS FROM spc_pipeline_win.py
pipeline_root = spcvars.pipeline_root
icons_dir = os.path.join(pipeline_root, 'icons')

# RENDER ASSET MANAGER WINDOW
class RenderAssetManagerWindow(pipeline_window):
	def __init__(self, parent=None):
		super(RenderAssetManagerWindow, self).__init__(parent)

		# MAIN WINDOW SETTINGS
		self.setWindowTitle('Render Asset Manager')
		self.resize(600, 450)
		hou_stylesheet = hou.qt.styleSheet()
		self.setStyleSheet(hou_stylesheet)

		# ICONS
		self.icon_up_arrow = QIcon(os.path.join(icons_dir, 'arrow_up_black.svg'))
		self.icon_down_arrow = QIcon(os.path.join(icons_dir, 'arrow_down_black.svg'))
		self.icon_current_arrow = QIcon(os.path.join(icons_dir, 'arrow_green_circle_right.svg'))
		self.icon_update_arrow = QIcon(os.path.join(icons_dir, 'update_arrow.svg'))

		# CONNECT BUTTONS
		self.button_ok.clicked.connect(self.ok_clicked)

		# MAIN DICTIONARY HOLDING ALL INFORMATION
		self.main_info_dict = {}

		# GET ALL THE RENDER ASSET NODES
		self.get_all_render_assets()

		# GATHER THE VERSIONS
		self.get_available_hda_versions()

		# BUILD THE INTERFACE ACCORDINGLY
		self.edit_ui()


	# SCAN CURRENT HIPFILE FOR RENDER ASSETS
	def get_all_render_assets(self):
		render_asset_nodes = []
		all_loaded_hdas = hou.hda.loadedFiles()
		all_loaded_definitions = [spc_render_asset_utils.get_definition_from_hda_file(i) for i in all_loaded_hdas]
		all_hda_user_info = [i.userInfo() for i in all_loaded_definitions]

		# CREATE A LIST WITH HDA NAMES OF ALL INSTALLED PIPELINE RENDER ASSETS
		# THEY CAN BE IDENTIFIED WITH THE EXTRA USER INFO BEING SET TO 'spacepipe_render_asset'
		render_asset_hda_names = []
		for index, i in enumerate(all_hda_user_info):
			if i == 'spacepipe_render_asset':
				render_asset_hda_names.append(all_loaded_definitions[index].nodeTypeName())

		# SEARCH FOR EXISTING NODES THAT MATCH THOSE NAMES
		all_nodes = hou.node("/").allSubChildren()
		for node in all_nodes:
			if node.type().name() in render_asset_hda_names:
				# BUT EXCLUDE THE ASSETS THAT HAVE THE 'to_be_exported' TAG
				all_parms = node.parms()
				if 'to_be_exported' not in [i.eval() for i in all_parms if i.name() == 'render_asset']:
					# IF THE NODE IS NOT ON /obj/ LEVEL, ADD THE NAME,
					# SO THIS IS VISIBLE (IMPORTANT IF MULTIPLE OF THE SAME ASSET EXIST IN SEVERAL LOCATIONS)
					if node.path().rsplit(node.name(), 1)[0] == '/obj/':
						node_display_name = node.name()
					else:
						parent = node.parent()
						prefix = parent.path().rsplit('/', 1)[-1]
						node_display_name = '../{}/{}'.format(prefix, node.name())

					# ADD TO THE LIST
					render_asset_nodes.append(node)
					# THE INFO DICTS NAME KEY WILL BE THE DISPLAY NAME
					# THE VALUE WILL BE ANOTHER DICTIONARY WITH ALL THE INFORMATION ABOUT THE NODE AND THE VERSIONS
					self.main_info_dict[node_display_name] = {'node_object':node}

		# SORT THE MAIN INFO DICT
		main_dict_sorted = sorted(list(self.main_info_dict.items()), key=lambda kv: kv[1])
		self.main_info_dict = collections.OrderedDict(main_dict_sorted)



	# GET THE AVAILABLE VERSIONS AND ADD THEM TO THE MAIN INFO DICTIONARY FOR EACH EXISTING NODE THAT IS ALREADY IN THERE
	def get_available_hda_versions(self):
		for node_name, node_info in list(self.main_info_dict.items()):

			hda_node = node_info.get('node_object')
			hda_file_path = spc_render_asset_utils.get_filepath_from_hda(hda_node)
			hda_definition_current = spc_render_asset_utils.get_definition_from_hda_file(hda_file_path)
			current_hda_def_name = hda_definition_current.nodeTypeName()
			current_hda_namespace, current_hda_name, current_hda_version = current_hda_def_name.split('::',2)

			# ADD CURRENT VERSION TO NODE INFO DICT
			node_info['current_version'] = {current_hda_version: hda_file_path}

			# SCAN THE HDA FILE PATH FOR OTHER AVAILABLE VERSIONS RELATED TO THE CURRENT HDA DEFINITION
			hda_dir = os.path.dirname(hda_file_path)
			all_versions_dict = {}
			if os.path.isdir(hda_dir):
				files_in_hda_dir = spc_utils.remove_unwanted_folders(os.listdir(hda_dir))
				for other_hda_file in files_in_hda_dir:
					other_hda_file_path = os.path.join(hda_dir, other_hda_file).replace('\\','/')
					other_hda_file_path = Path(other_hda_file_path).as_posix()
					other_hda_definition = spc_render_asset_utils.get_definition_from_hda_file(other_hda_file_path)
					other_hda_def_name = other_hda_definition.nodeTypeName()
					other_hda_namespace, other_hda_name, other_hda_version = other_hda_def_name.split('::', 2)
					other_hda_version = other_hda_definition.version()
					# ADD THE POTENTIAL VERSION INFO TO THE DICTIONARY IF NAMESPACE AND TYPENAME ARE MATCHING THE CURRENT HDA
					if other_hda_namespace == current_hda_namespace and other_hda_name == current_hda_name:
						all_versions_dict[other_hda_version] = other_hda_file_path

			# SORT THE VERSIONS DICTIONARY
			all_versions_dict_sorted = sorted(list(all_versions_dict.items()), key=lambda kv: kv[1])
			all_versions_dict_sorted = collections.OrderedDict(all_versions_dict_sorted)

			# ADD THE OTHER VERSIONS DICT TO THE NODE INFO DICT INSIDE THE MAIN INFO DICT
			node_info['all_versions'] = all_versions_dict_sorted


	# CREATE THE TABLE INTERFACE
	def edit_ui(self):
		# PROCESS THE MAIN INFO DICT AND BUILD THE INTERFACE
		row_count = len(list(self.main_info_dict.keys()))

		self.main_table_widget = QTableWidget()
		self.main_table_widget.setColumnCount(2)
		self.main_table_widget.setRowCount(row_count)
		header = self.main_table_widget.horizontalHeader()
		header.setSectionResizeMode(0, QHeaderView.Stretch)
		header.setSectionResizeMode(1, QHeaderView.ResizeToContents)
		self.main_table_widget.setHorizontalHeaderLabels(['Render Asset Node', '      Versions      '])
		self.main_layout.addWidget(self.main_table_widget)

		# POPULATE THE TABLE
		index = 0
		for hda_node_name, hda_data in list(self.main_info_dict.items()):

			hda_node_item = QTableWidgetItem(hda_node_name)
			self.main_table_widget.setItem(index, 0, hda_node_item)
			# DISABLE THESE ITEMS FOR EDITING OR SELECTING
			hda_node_item.setFlags(Qt.ItemIsEnabled)
			# CREATE AND POPULATE THE VERSION MENU
			current_hda_version = hda_data.get('current_version')
			all_hda_versions = hda_data.get('all_versions')
			version_dropdown = QComboBox()
			combobox_index = 0
			# START WITH HIGHEST VERSION AS VALUE UNTIL ACTUAL CURRENT INDEX WAS FOUND
			current_version_combobox_index = len(list(all_hda_versions.keys()))
			for hda_version, hda_version_path in list(all_hda_versions.items()):
				version_dropdown.addItem(hda_version)
				version_dropdown.setItemIcon(combobox_index, self.icon_down_arrow)
				# CURRENT VERSION
				if hda_version == list(current_hda_version.keys())[0]:
					current_version_combobox_index = combobox_index
					version_dropdown.setItemIcon(combobox_index, self.icon_current_arrow)
				# HIGHER THAN CURRENT VERSION
				if combobox_index > current_version_combobox_index:
					version_dropdown.setItemIcon(combobox_index, self.icon_up_arrow)
					# IF THERE IS A HIGHER VERSION THAN THE CURRENT, CHANGE THE CURRENT ITEMS ICON
					version_dropdown.setItemIcon(current_version_combobox_index, self.icon_update_arrow)

				combobox_index += 1

			# ADD THE COMBOBOX
			self.main_table_widget.setCellWidget(index, 1, version_dropdown)
			# SET CURRENT VERSION TO BE SELECTED IN THE COMBOBOX
			version_dropdown.setCurrentIndex(current_version_combobox_index)

			# INCREMENT THE INDEX FOR THE TABLE ROW
			index += 1

		# OPTIONS SECTION
		self.options_group = QGroupBox('Options')
		self.options_group.setContentsMargins(-2, -2, -2, -2)
		self.options_layout = QVBoxLayout()
		self.options_group.setLayout(self.options_layout)
		self.main_layout.addWidget(self.options_group)

		# RENAME NODES
		self.rename_nodes_checkbox = QCheckBox('Rename Nodes')
		self.rename_nodes_checkbox.setChecked(True)
		self.options_layout.addWidget(self.rename_nodes_checkbox)

		# VERBOSE MODE
		self.verbose_checkbox = QCheckBox('Verbose')
		self.verbose_checkbox.setChecked(True)
		self.options_layout.addWidget(self.verbose_checkbox)


	# EXECUTE ALL CHANGES
	def ok_clicked(self):
		verbose = self.verbose_checkbox.isChecked()

		# ITERATE OVER THE ROWS
		row_count = self.main_table_widget.rowCount()
		for row in range(row_count):
			if verbose:
				print('\n\n')
			hda_name = self.main_table_widget.item(row, 0).text()
			version_dropown = self.main_table_widget.cellWidget(row, 1)
			selected_version = version_dropown.currentText()

			# EXTRACT THE DATA FROM THE MAIN INFO DICTIONARY
			hda_node = self.main_info_dict.get(hda_name).get('node_object')
			current_hda_version_dict = self.main_info_dict.get(hda_name).get('current_version')
			current_hda_version_num = list(current_hda_version_dict.keys())[0]
			current_hda_version_file = list(current_hda_version_dict.values())[0]
			selected_hda_version_file = self.main_info_dict.get(hda_name).get('all_versions').get(selected_version)

			# UPDATE TO OTHER VERSION ONLY IF SELECTION IS NOT THE CURRENT VERSION ALREADY
			if selected_hda_version_file != current_hda_version_file:
				# CHECK IF DESIRED VERSION IS ALREADY INSTALLED
				# IF IT'S NOT, INSTALL IT BEFORE CHANGING THE NODE TYPE
				all_loaded_hdas = hou.hda.loadedFiles()
				if selected_hda_version_file not in all_loaded_hdas:
					###########  INSTALL SELECTED HDA VERSION  ##########
					if verbose:
						print(('INSTALLING HDA: {}'.format(selected_hda_version_file)))
					hou.hda.installFile(selected_hda_version_file)
					if verbose:
						print(('INSTALLED HDA: {}'.format(selected_hda_version_file)))

				###########  SET HDA NODE TO NEW NODE TYPE  ##########
				selected_hda_definition = spc_render_asset_utils.get_definition_from_hda_file(selected_hda_version_file)
				selected_hda_type_name = selected_hda_definition.nodeTypeName()
				# STORE THE PATH OF THE NODE THAT WILL BE CHANGED IN A STRING
				old_hda_node_path = str(hda_node.path())
				if verbose:
					print(('LINKING NODE TO HDA DEFINITION:\nNODE: {}\nVERSION: {}'.format(old_hda_node_path, selected_hda_type_name)))
				new_hda_node = hda_node.changeNodeType(selected_hda_type_name, keep_network_contents=False)
				if verbose:
					print(('LINKED NODE TO HDA DEFINITION:\nNODE: {}\nVERSION: {}'.format(new_hda_node.path(), selected_hda_type_name)))
				###########  SET NEW NAME  ##########
				if self.rename_nodes_checkbox.isChecked():
					new_hda_namespace, new_hda_typename, new_hda_version = selected_hda_type_name.split('::', 2)
					new_hda_name = '{}_{}_v{}'.format(new_hda_namespace, new_hda_typename, new_hda_version)
					if verbose:
						print(('RENAMING NODE:\nFROM: {}\nTO: {}'.format(new_hda_node.name(), new_hda_name)))
					new_hda_node.setName(new_hda_name, unique_name=True)


		# EXIT
		self.close()




# DELETE WINDOW IF ALREADY EXISTING
for entry in QApplication.allWidgets():
	if type(entry).__name__ == 'RenderAssetManagerWindow':
		entry.close()

# VARIABLE TO CALL FROM SHELF TOOL
window = RenderAssetManagerWindow()
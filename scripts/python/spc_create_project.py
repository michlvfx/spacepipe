import os
import shutil
import json
import spc_extract_dir_structure
import spc_global_pipeline_vars as spcvars
import spc_get_dir
import importlib
importlib.reload(spc_extract_dir_structure)
importlib.reload(spcvars)
importlib.reload(spc_get_dir)


def create_project(project_root, project_name, project_prefix, project_res_w, project_res_h, project_fps):
    # INIT
    return_dict = {}
    new_project_dir = os.path.join(project_root, project_name)

    # CHECK IF PROJECT ALREADY EXISTS
    if not os.path.isdir(new_project_dir):
        os.makedirs(new_project_dir)
        dirs_to_create = []
        # FETCH PROJECT DIRECTORIES TO CREATE FROM project_dir_structure.json
        proj_structure_json = spcvars.project_structure_json
        dirs_to_create = spc_extract_dir_structure.unpack_json_data(structure_json_file=proj_structure_json, root_dir=new_project_dir)
        # CREATE DIRECTORIES
        try:
            for i in dirs_to_create:
                os.makedirs(i)
        except:
            pass

        # COPY THE RENDER ASSET TEMPLATE FILE INTO THE PROJECT FOLDER
        source_render_asset_template_file = os.path.join(spcvars.pipeline_root, '_sourcefiles/houdini/empty_render_asset.hiplc')
        proj_render_asset_template_dir = spc_get_dir.get_project_dir_by_keyword(new_project_dir, 'render_asset_template')
        target_render_asset_template_file = os.path.join(proj_render_asset_template_dir, 'empty_render_asset.hiplc')
        try:
            shutil.copyfile(source_render_asset_template_file, target_render_asset_template_file)
        except:
            print('COULD NOT COPY RENDER ASSET TEMPLATE FILE!')

        # CREATE PROJECT SPECIFIC JSON FILES
        # project_settings.json
        project_settings_json = get_proj_settings_filepath(dirs_to_create)
        proj_settings_dict = {'project_root': new_project_dir, 'project_name': project_name, 'project_prefix': project_prefix,
                              'project_res_width': project_res_w, 'project_res_height': project_res_h, 'project_fps': project_fps}
        with open(project_settings_json, 'w') as f:
            json.dump(proj_settings_dict, f, indent=4, sort_keys=True)

        return_dict = {'success': 'Project "{}" created!'.format(project_name)}
    else:
        return_dict = {'duplicate': 'Project already exists'}
    # RETURN
    return return_dict


# BUILD PATH FOR PROJECT SETTINGS JSON FILE
def get_proj_settings_filepath(dirs_to_scan):
    target_file_path = ''
    project_settings_folder = '_project_settings'
    for i in dirs_to_scan:
        if i.count(project_settings_folder):
            target_dir = os.path.join(i.rsplit(project_settings_folder)[0], i)
            target_file_path = os.path.join(target_dir, 'project_settings.json')
            break

    return target_file_path

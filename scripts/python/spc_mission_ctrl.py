from PySide2.QtGui import *
from PySide2.QtCore import *
from PySide2.QtWidgets import *
import spc_pipeline_win as spcwin
import spc_create_project
import hou
import platform
import subprocess
import collections
import spc_read_settings as spcsettings
import spc_global_pipeline_vars as spcvars
import os
import getpass
import shutil
import spc_utils
import spc_create_dirs
import spc_get_dir
import spc_structurize
import spc_recent_info
import spc_render_asset_utils
import importlib
from pathlib import Path
importlib.reload(spc_create_project)
importlib.reload(spcsettings)
importlib.reload(spcvars)
importlib.reload(spcwin)
importlib.reload(spc_utils)
importlib.reload(spc_create_dirs)
importlib.reload(spc_get_dir)
importlib.reload(spc_structurize)
importlib.reload(spc_recent_info)
importlib.reload(spc_render_asset_utils)


# GLOBAL VARS
pipeline_window = spcwin.SpacepipeWindow  # BASIC PIPELINE WINDOW CLASS FROM spc_pipeline_win.py
pipeline_root = spcvars.pipeline_root
icons_dir = os.path.join(pipeline_root, 'icons')
icons_dir = Path(icons_dir).as_posix()
pipeline_temp = os.getenv('SPACEPIPE_TEMPDIR')
current_user = getpass.getuser().lower()
user_temp_dir = os.path.join(pipeline_temp, current_user)
user_temp_dir = Path(user_temp_dir).as_posix()
current_system_platform = platform.system()


# CUSTOM LIST WIDGET
class MissionCtrlList(QWidget):
	def __init__(self, groupname=None):
		super(MissionCtrlList, self).__init__()
		self.group_name = groupname
		self.item_info_dict = {}
		self.init_ui()

	def init_ui(self):
		main_vert = QVBoxLayout()
		main_vert.setContentsMargins(1, 4, 1, 4)
		groupbox = QGroupBox(self.group_name)
		self.box_vert = QVBoxLayout()
		self.box_vert.setContentsMargins(1, 3, 1, 3)
		groupbox.setLayout(self.box_vert)
		self.my_list = QListWidget()
		self.box_vert.addWidget(self.my_list)
		main_vert.addWidget(groupbox)
		groupbox.setContentsMargins(-10, -5, -10, -10)
		self.setLayout(main_vert)

	def get_list_object(self):
		return self.my_list

	def get_vert_object(self):
		return self.box_vert

	def clear(self):
		self.my_list.clear()
		self.item_info_dict = {}

	def insert_item(self, row, item_dict):
		self.item_info_dict.update(item_dict)
		item_object = QListWidgetItem(list(item_dict.keys())[0])
		self.my_list.insertItem(int(row), item_object)

	def add_item(self, item_dict, sort_by_name=False, sort_by_version=False):
		self.item_info_dict.update(item_dict)
		item_object = QListWidgetItem(list(item_dict.keys())[0])
		self.my_list.addItem(item_object)
		if sort_by_name:
			self.sort_by_name()
		elif sort_by_version:
			self.sort_by_version()

	def add_items(self, items_dict, sort_by_name=False, sort_by_version=False):
		self.item_info_dict.update(items_dict)
		item_names = list(items_dict.keys())
		for i in item_names:
			item_object = QListWidgetItem(i)
			self.my_list.addItem(item_object)
		if sort_by_name:
			self.sort_by_name()
		elif sort_by_version:
			self.sort_by_version()

	def get_all_items(self):
		items = []
		for index in range(self.my_list.count()):
			items.append(self.my_list.item(index))
		return items

	def sort_by_name(self):
		items = self.get_all_items()
		items_dict = {}
		for i in items:
			dict_item = {i.text(): self.item_info_dict.get(i.text())}
			items_dict.update(dict_item)
		sorted_info_dict = collections.OrderedDict(sorted(items_dict.items()))
		self.clear()
		self.add_items(sorted_info_dict)

	# NOT TESTED YET!
	def sort_by_version(self):
		items = self.get_all_items()
		items_dict = {}
		for i in items:
			version = i.text().rsplit('v', 1)[-1].zfill(3)
			dict_item = {version.join(i.text()): self.item_info_dict.get(i.text())}
		sorted_info_dict_raw = collections.OrderedDict(sorted(items_dict.items()))
		sorted_info_dict = {}
		for name, path in list(sorted_info_dict_raw.items()):
			corrected_name = name[3:]
			sorted_info_dict[corrected_name] = path
		self.clear()
		self.add_items(sorted_info_dict)


	def get_selected_item(self):
		try:
			selection = self.my_list.selectedItems()[0]
			return selection
		except IndexError:
			return None

	def remove(self, item_name):
		if type(item_name) == str:
			item_object = self.get_item(item_name)
		else:
			item_object = item_name
		self.my_list.removeItemWidget(item_object)
		# DELETE FROM INFO DICTIONARY
		del self.item_info_dict[item_name]

	def get_item(self, item_name):
		items = self.get_all_items()
		target_item_object = [i for i in items if i.text() == item_name][0]
		return target_item_object

	def set_selected_item(self, item_name):
		items = self.get_all_items()
		target_item_object = [i for i in items if i.text() == item_name][0]
		self.my_list.setCurrentItem(target_item_object)

	def set_all_items_color(self, color):
		items = self.get_all_items()
		for i in items:
			if color == 'green':
				i.setForeground(Qt.green)
			elif color == 'blue':
				i.setForeground(Qt.blue)
			if color == 'red':
				i.setForeground(Qt.red)

	def set_hda_icon_to_items(self):
		render_asset_icon = QIcon(os.path.join(icons_dir, 'box_closed_arrow.svg'))
		all_items = self.get_all_items()
		for i in all_items:
			i.setIcon(render_asset_icon)


# MAIN WINDOW
class MissionCtrlWindow(QDialog):
	def __init__(self, parent=None):
		super(MissionCtrlWindow, self).__init__(parent, Qt.WindowStaysOnTopHint)
		self.current_project_root = ''
		self.previous_type = 'shots'
		# MAIN WINDOW SETTINGS
		self.setWindowTitle('Mission CTRL')
		self.setWindowIcon(QIcon(os.path.join(icons_dir, 'rocket_small.png')))
		self.setAttribute(Qt.WA_DeleteOnClose)
		self.init_width = 850
		self.resize(self.init_width, 550)
		self.hou_stylesheet = hou.qt.styleSheet()
		self.setStyleSheet(self.hou_stylesheet)
		# ICONS
		self.shots_icon = QIcon(os.path.join(icons_dir, 'cam.png'))
		self.assets_icon = QIcon(os.path.join(icons_dir, 'teapot.png'))
		self.rnd_icon = QIcon(os.path.join(icons_dir, 'rnd.png'))
		self.add_icon = QIcon(os.path.join(icons_dir, 'add_dark.png'))
		self.up_icon = QIcon(os.path.join(icons_dir, 'up_dark.png'))
		self.import_icon = QIcon(os.path.join(icons_dir, 'down.png'))
		self.render_asset_icon = QIcon(os.path.join(icons_dir, 'box_closed_arrow.svg'))

		self.init_ui()

	# CREATE UI
	def init_ui(self):
		# MAIN
		main_layout = QVBoxLayout()
		main_layout.setSpacing(1)
		menu_bar_height = 30
		main_layout.setContentsMargins(10, menu_bar_height + 5, 10, 10)
		self.setLayout(main_layout)

		# MENUBAR
		main_menu_bar = QMenuBar(parent=self)
		# EDIT MENUBAR STYLESHEET BECAUSE HOUDINIS STYLESHEET LOOKS WEIRD HERE
		main_menu_bar.setStyleSheet('background: transparent; border: 0px')

		main_menu_bar.setFixedHeight(menu_bar_height)
		# MAC OS DOES USE HOUDINIS MENU BAR AND NOT HAVING THIS LINE WOULD KILL IT
		if current_system_platform.count('Darwin'):
			main_menu_bar.setNativeMenuBar(False)

		# FILE MENU
		file_menu = QMenu('File')
		file_menu.setStyleSheet(self.hou_stylesheet)

		main_menu_bar.addMenu(file_menu)

		# PROJECT SECTION
		project_groupbox = QGroupBox('Project')
		project_groupbox.setContentsMargins(-10, -10, -10, -10)
		project_groupbox.setFixedHeight(100)
		main_layout.addWidget(project_groupbox)
		project_toplayout = QVBoxLayout()
		project_toplayout.setSpacing(2)
		project_groupbox.setLayout(project_toplayout)
		# ROOT
		project_root_hori = QHBoxLayout()
		project_toplayout.addLayout(project_root_hori)
		project_root_label = QLabel('Root:')
		project_root_label.setFixedWidth(50)
		project_root_line = QLineEdit()
		project_root_line.setText(spcvars.global_project_root)
		project_root_line.setEnabled(False)
		project_root_hori.addWidget(project_root_label)
		project_root_hori.addWidget(project_root_line)
		# CURRENT
		project_current_hori = QHBoxLayout()
		project_toplayout.addLayout(project_current_hori)
		project_current_label = QLabel('Current:')
		project_current_label.setFixedWidth(50)
		self.project_current_menu = QComboBox()
		self.project_current_menu.currentIndexChanged.connect(self.current_project_changed)
		project_current_hori.addWidget(project_current_label)
		project_current_hori.addWidget(self.project_current_menu)

		# TYPE SECTION
		type_groupbox = QGroupBox('Type')
		type_groupbox.setContentsMargins(-10, -10, -10, -10)
		type_groupbox.setFixedHeight(50)
		type_hori = QHBoxLayout()
		type_hori.setSpacing(50)
		type_hori.addStretch()
		type_groupbox.setLayout(type_hori)
		main_layout.addWidget(type_groupbox)
		self.shots_radio = QRadioButton('Shot')
		self.shots_radio.setIcon(self.shots_icon)
		self.shots_radio.setChecked(True)
		self.shots_radio.clicked.connect(self.type_changed)
		type_hori.addWidget(self.shots_radio)
		self.assets_radio = QRadioButton('Asset')
		self.assets_radio.setIcon(self.assets_icon)
		self.assets_radio.clicked.connect(self.type_changed)
		type_hori.addWidget(self.assets_radio)
		self.rnd_radio = QRadioButton('RnD')
		self.rnd_radio.setIcon(self.rnd_icon)
		self.rnd_radio.clicked.connect(self.type_changed)
		type_hori.addWidget(self.rnd_radio)
		type_hori.addStretch()

		# SELECTION LISTS SECTION
		selection_groupbox = QGroupBox('Selection')
		selection_groupbox.setContentsMargins(-10, -10, -10, -10)
		selection_vert = QVBoxLayout()
		selection_vert.setSpacing(0)
		selection_hori = QHBoxLayout()
		selection_hori.setSpacing(0)
		selection_vert.addLayout(selection_hori)
		selection_groupbox.setLayout(selection_vert)
		main_layout.addWidget(selection_groupbox)

		# NAME LIST
		self.name_list = MissionCtrlList('Name')
		self.name_list_object = self.name_list.get_list_object()
		self.name_list_object.itemSelectionChanged.connect(self.update_task_list)
		self.name_list_object.itemDoubleClicked.connect(lambda: self.item_double_clicked(self.name_list))
		selection_hori.addWidget(self.name_list)
		self.name_list.setMinimumWidth(220)
		name_list_vert = self.name_list.get_vert_object()
		self.create_shot_asset_rnd_button = QPushButton('Create New...')
		self.create_shot_asset_rnd_button.setIcon(self.add_icon)
		self.create_shot_asset_rnd_button.clicked.connect(self.create_shot_asset_rnd_button_clicked)
		name_list_vert.addWidget(self.create_shot_asset_rnd_button)

		# TASK LIST
		self.task_list = MissionCtrlList('Task')
		self.task_list_object = self.task_list.get_list_object()
		self.task_list_object.itemSelectionChanged.connect(self.update_file_list)
		self.task_list_object.itemDoubleClicked.connect(lambda: self.item_double_clicked(self.task_list))
		selection_hori.addWidget(self.task_list)
		self.task_list.setMinimumWidth(150)
		task_list_vert = self.task_list.get_vert_object()
		self.create_task_button = QPushButton('Create New...')
		self.create_task_button.setIcon(self.add_icon)
		self.create_task_button.clicked.connect(self.create_task_button_clicked)
		task_list_vert.addWidget(self.create_task_button)

		# FILE LIST
		self.file_list = MissionCtrlList('File')
		self.file_list_object = self.file_list.get_list_object()
		self.file_list_object.itemSelectionChanged.connect(self.update_version_list)
		self.file_list_object.itemDoubleClicked.connect(lambda: self.item_double_clicked(self.file_list))
		selection_hori.addWidget(self.file_list)
		self.file_list.setMinimumWidth(150)
		file_list_vert = self.file_list.get_vert_object()
		self.create_file_button = QPushButton('Create New...')
		self.create_file_button.setIcon(self.add_icon)
		self.create_file_button.clicked.connect(self.create_file_button_clicked)
		file_list_vert.addWidget(self.create_file_button)

		# VERSION LIST
		self.version_list = MissionCtrlList('Version')
		self.version_list_object = self.version_list.get_list_object()
		self.version_list_object.itemSelectionChanged.connect(self.update_hda_list)
		self.version_list_object.itemDoubleClicked.connect(lambda: self.item_double_clicked(self.version_list))
		selection_hori.addWidget(self.version_list)
		self.version_list.setMaximumWidth(125)
		version_list_vert = self.version_list.get_vert_object()
		self.new_version_button = QPushButton('Version Up')
		self.new_version_button.setIcon(self.up_icon)
		self.new_version_button.clicked.connect(self.new_version_button_clicked)
		version_list_vert.addWidget(self.new_version_button)

		# HDA LIST
		self.hda_list = MissionCtrlList('Render Assets')
		self.hda_list_object = self.hda_list.get_list_object()
		self.hda_list_object.itemSelectionChanged.connect(self.hda_list_selection_changed)
		self.hda_list_object.itemDoubleClicked.connect(lambda: self.item_double_clicked(self.hda_list))
		selection_hori.addWidget(self.hda_list)
		self.hda_list.setMinimumWidth(90)
		hda_list_vert = self.hda_list.get_vert_object()
		self.import_hda_button = QPushButton('Import HDA')
		self.import_hda_button.setIcon(self.import_icon)
		self.import_hda_button.clicked.connect(self.import_hda_button_clicked)
		hda_list_vert.addWidget(self.import_hda_button)


		# CLOSE BUTTON BOTTOM
		main_close_button = QPushButton('Close')
		main_layout.addWidget(main_close_button)
		main_close_button.clicked.connect(self.close_ui)

		# POPULATE WIDGETS
		self.populate_main_menus(file_menu)
		self.populate_project_current_menu()

		# SELECT RECENT
		self.select_recent()

	# POPULATE THE MAIN MENUS
	def populate_main_menus(self, menu):
		# PROJECT MENU
		if menu.title() == 'File':
			new_proj_action = QAction('Create New Project...', self)
			new_proj_action.triggered.connect(self.new_proj_clicked)
			menu.addAction(new_proj_action)

	# POPULATE CURRENT PROJECT MENU
	def populate_project_current_menu(self):
		project_root = spcvars.global_project_root
		if os.path.isdir(project_root):
			project_folders = os.listdir(project_root)
			project_folders = spc_utils.remove_unwanted_folders(project_folders)
			# AT LEAST ONE PROJECT EXISTING
			if len(project_folders) > 0:
				# SORT BY DATE
				full_path_folders = [os.path.join(project_root, i) for i in project_folders]
				date_sorted_folders = [os.path.basename(i) for i in self.sort_by_date(full_path_folders)]
				for i in date_sorted_folders:
					self.project_current_menu.addItem(i)
			# NO PROJECT FOUND
			else:
				self.disable_buttons(all_buttons=True)
		# INVALID PROJECT ROOT DIRECTORY
		else:
			hou.ui.displayMessage('Project Root does not exist!', title='Warning',
								  severity=hou.severityType.Warning, details=project_root)
			self.disable_buttons(all_buttons=True)

	# DISABLE BUTTONS
	def disable_buttons(self, all_buttons=False, buttons=None):
		if all_buttons:
			self.create_shot_asset_rnd_button.setDisabled(True)
			self.create_task_button.setDisabled(True)
			self.create_file_button.setDisabled(True)
			self.new_version_button.setDisabled(True)
			self.import_hda_button.setDisabled(True)
		elif all_buttons is False and buttons is not None:
			for button in buttons:
				button.setDisabled(True)

	# ENABLE BUTTONS
	def enable_buttons(self, all_buttons=False, buttons=None):
		if all_buttons:
			self.create_shot_asset_rnd_button.setEnabled(True)
			self.create_task_button.setEnabled(True)
			self.create_file_button.setEnabled(True)
			self.new_version_button.setEnabled(True)
			self.import_hda_button.setEnabled(True)
		elif all_buttons is False and buttons is not None:
			for button in buttons:
				button.setEnabled(True)

	# CURRENT PROJECT MENU CHANGED
	def current_project_changed(self):
		current_project_name = self.project_current_menu.currentText()
		if current_project_name:
			self.current_project_root = os.path.join(spcvars.global_project_root, current_project_name)
			self.current_project_root = Path(self.current_project_root).as_posix()
			self.update_name_list()
			self.enable_buttons(all_buttons=False, buttons=[self.create_shot_asset_rnd_button])
		else:
			self.disable_buttons(all_buttons=True)

	# TYPE RADIO BUTTONS CHANGED
	def type_changed(self):
		current_type = self.get_current_type()
		if current_type is not self.previous_type:
			self.clean_selection_lists(clear_all=True)
			self.previous_type = current_type
			self.update_name_list()

	# SORT FILES BY CREATION TIME
	def sort_by_date(self, items):
		items.sort(key=os.path.getctime)
		items.reverse()
		return items

	# CLOSE UI
	def close_ui(self):
		self.close()

	# NEW PROJECT CLICKED
	def new_proj_clicked(self):
		new = NewProjectWindow()
		new.the_signal.connect(self.refresh_ctrl_hub_window)

	# GET TYPE
	def get_current_type(self):
		current_type = ''
		if self.shots_radio.isChecked():
			current_type = 'shots'
		elif self.assets_radio.isChecked():
			current_type = 'assets'
		elif self.rnd_radio.isChecked():
			current_type = 'rnd'
		return current_type

	# NEW TASK BUTTON CLICKED
	def create_task_button_clicked(self):
		name_list_item = self.name_list.get_selected_item()
		name_list_item_name = name_list_item.text()
		name_list_item_path = self.name_list.item_info_dict.get(name_list_item_name).get('path')
		root_dir = name_list_item_path
		new = ''
		existing_items = [i.text() for i in self.task_list.get_all_items()]
		new = NewTaskWindow(root_dir, existing_tasks=existing_items)
		# UPDATE LISTS
		new.the_signal.connect(lambda: self.update_task_list(select_newest=True))

	# NEW SHOT / ASSET / RND BUTTON CLICKED
	def create_shot_asset_rnd_button_clicked(self):
		current_type = self.get_current_type()
		existing_items = [i.text() for i in self.name_list.get_all_items()]
		new = ''

		# NEW SHOT
		if current_type == 'shots':
			new = NewShotWindow(self.current_project_root, existing_shots=existing_items)
		# NEW ASSET
		elif current_type == 'assets':
			new = NewAssetWindow(self.current_project_root, existing_assets=existing_items)
		# NEW RND
		elif current_type == 'rnd':
			new = NewRnDWindow(self.current_project_root, existing_rnds=existing_items)

		# UPDATE LISTS
		new.the_signal.connect(lambda: self.update_name_list(select_newest=True))

	# NEW FILE BUTTON CLICKED
	def create_file_button_clicked(self):
		task_list_item = self.task_list.get_selected_item()
		task_list_item_name = task_list_item.text()
		task_list_item_path = self.task_list.item_info_dict.get(task_list_item_name).get('path')
		root_dir = task_list_item_path
		new = ''
		existing_items = [i.text() for i in self.file_list.get_all_items()]

		new = NewFileWindow(self.current_project_root, root_dir, existing_files=existing_items)
		# UPDATE LISTS
		new.the_signal.connect(lambda: self.update_file_list(select_newest=True))

	# NEW VERSION BUTTON CLICKED
	def new_version_button_clicked(self):
		selected_file_item = self.file_list.get_selected_item()
		file_item_name = selected_file_item.text()
		selected_file_path = self.file_list.item_info_dict.get(file_item_name).get('path')
		selected_version_item = self.version_list.get_selected_item()
		current_file_path = hou.hipFile.path()

		# CHECK IF CURRENT FILE IS INSIDE OF PIPELINE STRUCTURE
		try:
			current_file_name = spc_structurize.structurize_file_name(current_file_path).get('name_short_versionless')
			current_task_name = spc_structurize.structurize_path(current_file_path).get('task')
			current_name = spc_structurize.structurize_path(current_file_path).get('name')
			current_type = spc_structurize.structurize_path(current_file_path).get('type').lower()
		except TypeError:
			current_file_name = 'out of pipe'
			current_task_name = 'out of pipe'
			current_name = 'out of pipe'
			current_type = 'out of pipe'

		selected_file_name = self.file_list.get_selected_item().text()
		selected_task_name = self.task_list.get_selected_item().text()
		selected_name = self.name_list.get_selected_item().text()
		selected_type = self.get_current_type()

		# CURRENT FILE OUTSIDE OF PIPELINE STRUCTURE
		if current_file_name == 'out of pipe':
			user_input = hou.ui.displayMessage('Are you sure you want to save from outside the pipeline into \n {} "/{}/{}/"?'.format(selected_type, selected_name, selected_task_name),
											   title='Sure?', buttons=['Yes', 'No'], severity=hou.severityType.Warning)
			# SAVE INTO PIPELINE
			if user_input == 0:
				new_version_path = self.create_new_version_path(selected_file_path)
			else:
				return

		# FILENAME IS THE SAME AS SELECTED
		elif current_file_name == selected_file_name and current_task_name == selected_task_name and current_name == selected_name and current_type in selected_type:
			new_version_path = self.create_new_version_path(selected_file_path)

		# CURRENT FILE IS DIFFERENT TO SELECTED
		else:
			user_input = hou.ui.displayMessage('Are you sure you want to save from {} "/{}/{}/" into {} "/{}/{}/"?'.format(current_type, current_name, current_task_name,
											selected_type, selected_name, selected_task_name), title='Sure?', buttons=['Yes', 'No'], severity=hou.severityType.Warning)
			# SAVE INTO NEW CONTEXT
			if user_input == 0:
				new_version_path = self.create_new_version_path(selected_file_path)
			else:
				return

		# CHECK FOR RENDER ASSET HDA CORRESPONDING TO CURRENT VERSION
		current_render_asset_hda = spc_render_asset_utils.get_current_render_asset_from_hip()

		# DO IT
		if new_version_path:
			hou.hipFile.save(new_version_path)
			self.update_version_list(select_newest=True)
			# WRITE RECENT FILE INFO
			recent_dir = user_temp_dir
			spc_recent_info.write(recent_dir, new_version_path)
			# CONSOLIDATE RENDER ASSET APPEARANCE IF NECESSARY
			if current_render_asset_hda:
				hda_file_path = spc_render_asset_utils.get_filepath_from_hda(current_render_asset_hda)
				render_asset_status = spc_render_asset_utils.get_render_asset_status(hda_file_path=hda_file_path)
				if render_asset_status == 'published' or render_asset_status == 'PUBLISHED':
					# CHANGE THE APPEARANCE TO "published_but_not_current"
					spc_render_asset_utils.update_render_asset_appearance(current_render_asset_hda, hda_file_path, 'published_but_not_current')
			self.close()


	# IMPORT HDA BUTTON CLICKED
	def import_hda_button_clicked(self):
		selected_hda_item = self.hda_list.get_selected_item()
		hda_item_name = selected_hda_item.text()
		# GET THE FULL PATH TO THE HDA FILE ON DISK
		selected_hda_path = self.hda_list.item_info_dict.get(hda_item_name).get('path')
		# ALSO READ THE HDA DEFINITION AND EXTRACT THE NODE'S NAME TO BE CREATED AFTER THE INSTALL
		hda_definition = hou.hda.definitionsInFile(selected_hda_path)[-1]
		hda_nodetype_name = hda_definition.nodeTypeName()

		# INSTALL THE HDA
		hou.hda.installFile(selected_hda_path)

		# CREATE THE HDA
		obj_network = hou.node('/obj/')
		hda_version = hda_definition.version()
		hda_node_name = hda_nodetype_name.replace('::', '_').rsplit('_', 1)[0] + '_v{}'.format(hda_version)
		created_hda_node = obj_network.createNode(hda_nodetype_name, node_name=hda_node_name, exact_type_name=True)

		# ARRANGE NEWLY CREATED NODE
		spc_render_asset_utils.set_node_color(created_hda_node, 'published')
		created_hda_node.setUserData('nodeshape', 'bulge_down')
		node_pos = [-18, -5]
		created_hda_node.setPosition(node_pos)
		spc_render_asset_utils.set_focus_to_hda_node(created_hda_node)

		hou.hda.reloadNamespaceOrder()

		self.close()


	# CREATE NEW VERSION PATH
	def create_new_version_path(self, file_dir):

		file_item_name = self.file_list.get_selected_item().text()
		main_structure_dict = spc_structurize.structurize_path(file_dir)
		project_prefix = main_structure_dict.get('project_prefix')
		type_name = main_structure_dict.get('name')
		task = main_structure_dict.get('task')
		file_ext = os.path.splitext(hou.hipFile.path())[-1]
		highest_version = sorted([i.text() for i in self.version_list.get_all_items()])[-1]
		highest_version_number = highest_version.split('v')[-1]
		new_version = 'v' + str(int(highest_version_number) + 1).zfill(3)
		new_basename = '_'.join([project_prefix, type_name, task, file_item_name, new_version]) + file_ext
		new_version_path = os.path.join(file_dir, new_basename)
		new_version_path = Path(new_version_path).as_posix()

		return new_version_path

	# REFRESH MAIN WINDOW
	def refresh_ctrl_hub_window(self):
		importlib.reload(spc_get_dir)
		self.clean_ctrl_hub_project()
		self.clean_selection_lists(clear_all=True)
		self.populate_project_current_menu()

	# CLEAN CURRENT PROJECT MENU
	def clean_ctrl_hub_project(self):
		try:
			self.project_current_menu.clear()
		except:
			pass

	# CLEAN MAIN WINDOW LISTS
	def clean_selection_lists(self, clear_all=False, lists_to_clean=None):
		if not clear_all:
			for i in lists_to_clean:
				i.clear()
		else:
			self.name_list.clear()
			self.task_list.clear()
			self.file_list.clear()
			self.version_list.clear()
			self.hda_list.clear()
		importlib.reload(spc_get_dir)

	# UPDATE MAIN LIST
	def update_name_list(self, select_newest=False):
		if os.path.isdir(self.current_project_root):
			importlib.reload(spc_get_dir)
			item_names = []
			dir_to_scan = ''
			# CLEAN ALL LISTS
			self.clean_selection_lists(clear_all=True)
			# DISABLE BUTTONS
			self.disable_buttons(buttons=[self.create_file_button, self.new_version_button])
			current_project_root = self.current_project_root
			# TYPE
			current_type = self.get_current_type()
			if current_type == 'shots':
				# GET ALL PRESENT SHOTS IN SHOTS DIRECTORY
				dir_to_scan = spc_get_dir.get_shots_dir_main(current_project_root)
			elif current_type == 'assets':
				# GET ALL PRESENT ASSETS IN ASSETS DIRECTORY
				dir_to_scan = spc_get_dir.get_assets_dir_main(current_project_root)
			elif current_type == 'rnd':
				# GET ALL PRESENT RND ASSETS IN RND DIRECTORY
				dir_to_scan = spc_get_dir.get_rnd_dir_main(current_project_root)
			# EXISTS?
			if os.path.isdir(dir_to_scan):
				item_names = os.listdir(dir_to_scan)
				item_names = spc_utils.remove_unwanted_folders(item_names)
				if len(item_names) > 0:
					items_dict = {}
					newest = ''
					latest_creation_time = 0
					for name in item_names:
						full_path = os.path.join(dir_to_scan, name)
						full_path = Path(full_path).as_posix()
						creation_time = os.path.getctime(full_path)
						if creation_time > latest_creation_time:
							latest_creation_time = creation_time
							newest = name
						item_dict = {name: {'path': full_path}}
						items_dict.update(item_dict)

					# ADD TO LIST WIDGET
					self.name_list.add_items(items_dict, sort_by_name=True)
					# SELECT NEWEST
					if select_newest:
						self.name_list.set_selected_item(newest)
				# NO SHOT / ASSET / RND ITEM FOUND
				else:
					self.disable_buttons(buttons=[self.create_task_button, self.create_file_button, self.new_version_button])
				# UPDATE NEXT LISTS AND BUTTONS
				self.update_task_list()
			else:
				self.clean_selection_lists(clear_all=True)

	# UPDATE TASK LIST
	def update_task_list(self, select_newest=False):
		selected_name_item = self.name_list.get_selected_item()
		items_names_raw = []
		item_names = []
		self.clean_selection_lists(lists_to_clean=[self.task_list, self.file_list, self.version_list, self.hda_list])
		if selected_name_item:
			self.disable_buttons(buttons=[self.create_file_button, self.new_version_button])
			self.enable_buttons(buttons=[self.create_shot_asset_rnd_button, self.create_task_button])
			selected_name = selected_name_item.text()
			selected_name_path = self.name_list.item_info_dict.get(selected_name).get('path')
			# GET ALL FOLDERS IN TARGET DIR
			if os.path.isdir(selected_name_path):
				item_names_raw = spc_utils.remove_unwanted_folders(os.listdir(selected_name_path))
				# ONLY CHECK FOR DIRECTORIES THAT HAVE A 'houdini' SUBDIRECTORY INSIDE (eg a COMP task wouldn't have one typically)
				for name in item_names_raw:
					full_path = os.path.join(selected_name_path, name)
					full_path = Path(full_path).as_posix()
					if os.path.isdir(os.path.join(full_path, 'houdini')):
						item_names.append(name)
				if len(item_names) > 0:
					items_dict = {}
					newest = ''
					latest_creation_time = 0
					for name in item_names:
						full_path = os.path.join(selected_name_path, name)
						full_path = os.path.join(full_path, 'houdini')
						full_path = Path(full_path).as_posix()
						creation_time = os.path.getctime(full_path)
						if creation_time > latest_creation_time:
							latest_creation_time = creation_time
							newest = name
						item_dict = {name: {'path': full_path}}
						items_dict.update(item_dict)

					# ADD TO LIST WIDGET
					self.task_list.add_items(items_dict, sort_by_name=True)
					# SELECT NEWEST
					if select_newest:
						self.task_list.set_selected_item(newest)
				# NO TASK ITEM FOUND
				else:
					self.disable_buttons(buttons=[self.create_file_button, self.new_version_button])
				# UPDATE NEXT LISTS AND BUTTONS
				self.update_file_list()
		# NOTHING SELECTED
		else:
			self.disable_buttons(buttons=[self.create_file_button, self.new_version_button])

	# UPDATE FILE LIST
	def update_file_list(self, select_newest=False):
		selected_task_item = self.task_list.get_selected_item()
		# TASK CHANGED
		if selected_task_item:
			self.clean_selection_lists(lists_to_clean=[self.hda_list, self.version_list, self.file_list])
			self.disable_buttons(buttons=[self.new_version_button, self.import_hda_button])
		items_names = []
		if selected_task_item:
			self.enable_buttons(buttons=[self.create_shot_asset_rnd_button, self.create_task_button, self.create_file_button])
			selected_task = selected_task_item.text()
			selected_task_path = self.task_list.item_info_dict.get(selected_task).get('path')

			# GET ALL FOLDERS IN TARGET DIR
			if os.path.isdir(selected_task_path):
				item_names_raw = spc_utils.remove_unwanted_folders(os.listdir(selected_task_path))
				# ONLY CONSIDER .hip, .hiplc or .hipnc FILES
				valid_hip_exts = ['.hip', '.hiplc', '.hipnc']
				hip_item_names = []
				for i in item_names_raw:
					if os.path.splitext(i)[-1] in valid_hip_exts:
						hip_item_names.append(i)
				# HIP FILES FOUND
				if len(hip_item_names) > 0:
					items_dict = {}
					newest = ''
					latest_creation_time = 0
					for name in hip_item_names:
						full_path = os.path.join(selected_task_path, name)
						full_path = Path(full_path).as_posix()
						display_name = spc_structurize.structurize_file_name(full_path).get('name_short_versionless')
						version = spc_structurize.structurize_file_name(full_path).get('version')
						full_dir = selected_task_path
						creation_time = os.path.getctime(full_path)
						if creation_time > latest_creation_time:
							latest_creation_time = creation_time
							newest = display_name
						item_dict = {display_name: {'path': full_dir}}
						items_dict.update(item_dict)

					# ADD TO LIST WIDGET
					self.file_list.add_items(items_dict, sort_by_name=True)
					# SELECT NEWEST
					if select_newest:
						self.file_list.set_selected_item(newest)
						self.update_version_list(select_newest=select_newest)

				# NO HIP FILE FOUND
				else:
					self.disable_buttons(buttons=[self.new_version_button])

		# NO TASK SELECTED
		else:
			self.disable_buttons(buttons=[self.create_file_button, self.new_version_button, self.import_hda_button])

	# UPDATE VERSION LIST
	def update_version_list(self, select_newest=False):
		selected_file_item = self.file_list.get_selected_item()
		# FILE CHANGED
		if selected_file_item:
			self.clean_selection_lists(lists_to_clean=[self.version_list, self.hda_list])
		items_names = []
		if selected_file_item:
			self.enable_buttons(buttons=[self.create_shot_asset_rnd_button, self.create_task_button, self.create_file_button, self.new_version_button])
			selected_file = selected_file_item.text()
			selected_file_path = self.file_list.item_info_dict.get(selected_file).get('path')

			# GET ALL FOLDERS IN TARGET DIR
			if os.path.isdir(selected_file_path):
				item_names_raw = spc_utils.remove_unwanted_folders(os.listdir(selected_file_path))
				# ONLY CONSIDER .hip, .hiplc or .hipnc FILES
				valid_hip_exts = ['.hip', '.hiplc', '.hipnc']
				hip_item_names = []
				for i in item_names_raw:
					if os.path.splitext(i)[-1] in valid_hip_exts:
						item_path = os.path.join(selected_file_path, i)
						item_path = Path(item_path).as_posix()
						current_file_name = spc_structurize.structurize_file_name(item_path).get('name_short_versionless')
						selected_file_name = self.file_list.get_selected_item().text()
						# FILENAME IS THE SAME
						if current_file_name == selected_file_name:
							hip_item_names.append(i)
				# HIP FILES FOUND
				if len(hip_item_names) > 0:
					items_dict = {}
					newest = ''
					latest_creation_time = 0
					for name in hip_item_names:
						full_path = os.path.join(selected_file_path, name)
						full_path = Path(full_path).as_posix()
						version = spc_structurize.structurize_file_name(full_path).get('version')
						creation_time = os.path.getctime(full_path)
						if creation_time > latest_creation_time:
							latest_creation_time = creation_time
							newest = version
						item_dict = {version: {'path': full_path}}
						items_dict.update(item_dict)

					# ADD TO LIST WIDGET
					self.version_list.add_items(items_dict, sort_by_name=True)
					# SELECT NEWEST
					if select_newest:
						self.version_list.set_selected_item(newest)
		# NO FILE SELECTED
		else:
			self.disable_buttons(buttons=[self.new_version_button, self.import_hda_button])


	# UPDATE HDA LIST
	def update_hda_list(self, select_newest=False):
		selected_version_item = self.version_list.get_selected_item()
		selected_type_item = self.file_list.get_selected_item()
		# VERSION CHANGED
		if selected_version_item and selected_type_item:
			self.clean_selection_lists(lists_to_clean=[self.hda_list])
			self.enable_buttons(buttons=[self.create_shot_asset_rnd_button, self.create_task_button, self.create_file_button, self.new_version_button])
			selected_type = selected_type_item.text()
			selected_type_path = self.file_list.item_info_dict.get(selected_type).get('path').rsplit('houdini')[0]
			selected_version_item = self.version_list.get_selected_item()
			selected_version = selected_version_item.text()
			selected_version_path = self.version_list.item_info_dict.get(selected_version).get('path').replace('\\', '/')


			# CREATE THE PSEUDO INFORMATION FOR THE CURRENTLY SELECTED VERSION AND SEE IF THIS HDA EXISTS ON DISK
			hda_info_dict = spc_render_asset_utils.create_hda_info_from_hip(hipfile=selected_version_path)
			selected_hda_full_path = hda_info_dict.get('hda_full_path').replace('\\', '/')

			# CORRESPONDING HDA FILE FOUND
			if os.path.isfile(selected_hda_full_path):
				hda_definition = spc_render_asset_utils.get_definition_from_hda_file(selected_hda_full_path)

				# CHECK IF HDA IS PUBLISHED
				comment = hda_definition.comment()
				if comment == 'PUBLISHED':
					# ADD TO LIST WIDGET
					item_dict = {'HDA': {'path': selected_hda_full_path}}
					self.hda_list.add_item(item_dict)


				# COLOR HDA LIST ITEMS IN GREEN
				self.hda_list.set_all_items_color('green')

				# SET RENDER ASSET ICON
				self.hda_list.set_hda_icon_to_items()

		# NO VERSION SELECTED
		else:
			self.disable_buttons(buttons=[self.import_hda_button])


	# HDA LIST SELECTION CHANGED
	def hda_list_selection_changed(self):
		selected_version_item = self.version_list.get_selected_item()
		if selected_version_item:
			selected_version = selected_version_item.text()
			selected_version_path = self.version_list.item_info_dict.get(selected_version).get('path').replace('/', '\\')
			# CHECK IF SELECTED VERSION IS THE CURRENTLY OPENED FILE
			# IF YES, IT SHOULDN'T BE POSSIBLE TO IMPORT THE HDA BECAUSE IT'S THE ONE FROM THIS FILE
			opened_file_path = hou.getenv('HIPFILE').replace('/', '\\')

			if not selected_version_path == opened_file_path:
				selected_hda_item = self.hda_list.get_selected_item()
				if selected_hda_item:
					self.enable_buttons(buttons=[self.import_hda_button])
				else:
					self.disable_buttons(buttons=[self.import_hda_button])
			else:
				self.disable_buttons(buttons=[self.import_hda_button])
		else:
			self.disable_buttons(buttons=[self.import_hda_button])



	# ITEM DOUBLE CLICKED
	def item_double_clicked(self, target_list):
		clicked_item_name = target_list.get_selected_item().text()
		clicked_path = target_list.item_info_dict.get(clicked_item_name).get('path')
		if os.path.isdir(clicked_path):
			try:
				# WINDOWS
				if current_system_platform == 'Windows':
					os.startfile(clicked_path)
				# MACOS
				elif current_system_platform == 'Darwin':
					subprocess.Popen(["open", clicked_path])
				# LINUX
				else:
					subprocess.Popen(["xdg-open", clicked_path])
			except:
				print(('COULD NOT OPEN FILEPATH:\n{}'.format(clicked_path)))
		# OPEN FILE
		elif os.path.isfile(clicked_path):
			if os.path.splitext(clicked_path)[-1].count('hip'):
				hou.hipFile.load(clicked_path)
				# WRITE RECENT FILE INFO
				recent_dir = user_temp_dir
				spc_recent_info.write(recent_dir, clicked_path)
				self.close()


	# SELECT RECENT
	def select_recent(self):
		# GET LATEST RECENT FILE
		recent_file_path = spc_recent_info.get_latest(user_temp_dir)
		recent_dict = spc_recent_info.read(recent_file_path)

		# GATHER DATA
		recent_project = os.path.splitext(os.path.basename(recent_file_path))[0]
		recent_type = recent_dict.get('type')
		recent_name = recent_dict.get('name')
		recent_task = recent_dict.get('task')
		recent_file = recent_dict.get('file')
		recent_version = recent_dict.get('version')

		try:
			# APPLY SELECTIONS
			self.project_current_menu.setCurrentText(recent_project)
			if recent_type == 'SHOT':
				self.shots_radio.setChecked(True)
			elif recent_type == 'ASSET':
				self.assets_radio.setChecked(True)
			elif recent_type == 'RND':
				self.rnd_radio.setChecked(True)

			self.type_changed()

			self.name_list.set_selected_item(recent_name)
			self.task_list.set_selected_item(recent_task)
			self.file_list.set_selected_item(recent_file)
			self.version_list.set_selected_item(recent_version)
		except:
			print('COULD NOT SET RECENT FILE')


# NEW FILE WINDOW
class NewFileWindow(pipeline_window):
	the_signal = Signal(str)

	def __init__(self, project_root, root_dir, existing_files=None):
		super(NewFileWindow, self).__init__()
		self.current_project_root = project_root
		self.root_dir = root_dir
		self.existing_files = existing_files
		self.file_icon = QIcon(os.path.join(icons_dir, 'file.png'))
		self.empty_file_icon = QIcon(os.path.join(icons_dir, 'emptyfile.png'))
		# MAIN WINDOW SETTINGS
		self.setWindowTitle('Create New File')
		self.resize(300, 200)
		hou_stylesheet = hou.qt.styleSheet()
		self.setStyleSheet(hou_stylesheet)

		self.edit_ui()

	# EDIT UI
	def edit_ui(self):
		# OVERWRITE EXISTING WIDGETS
		self.main_group.setTitle('Settings')
		self.button_ok.setText('Create')
		self.button_ok.clicked.connect(self.button_create_clicked)
		self.setWindowIcon(QIcon(os.path.join(icons_dir, 'add_dark.png')))

		label_width = 65

		# MODE
		mode_groupbox = QGroupBox('Mode')
		mode_groupbox.setFixedHeight(50)
		mode_hori = QHBoxLayout()
		mode_groupbox.setLayout(mode_hori)
		self.main_layout.addWidget(mode_groupbox)
		self.new_file_radio = QRadioButton('New File')
		self.new_file_radio.setIcon(self.empty_file_icon)
		self.new_file_radio.setChecked(True)
		self.from_current_file_radio = QRadioButton('From Current File')
		self.from_current_file_radio.setIcon(self.file_icon)
		mode_hori.addStretch()
		mode_hori.addWidget(self.new_file_radio)
		mode_hori.addWidget(self.from_current_file_radio)
		mode_hori.addStretch()

		# FILE NAME
		file_name_hori = QHBoxLayout()
		self.main_layout.addLayout(file_name_hori)
		self.file_name_label = QLabel('Name:')
		file_name_tooltip = 'Name can be a mix of alphabetical letters.'
		self.file_name_label.setToolTip(file_name_tooltip)
		self.file_name_label.setFixedWidth(label_width)
		self.file_name_line = QLineEdit()
		self.file_name_line.setToolTip(file_name_tooltip)
		self.file_name_line.textEdited.connect(lambda: reset_stylesheet([self.file_name_line, self.file_name_label]))
		self.file_name_line.textEdited.connect(lambda: self.conform_user_input('file_name'))
		file_name_hori.addWidget(self.file_name_label)
		file_name_hori.addWidget(self.file_name_line)

		# OPEN
		self.show()

	# CONFORM USER INPUT
	def conform_user_input(self, option):
		if option == 'file_name':
			try:
				user_text = self.task_name_line.text()
				corrected_text = user_text
				# SET
				self.task_name_line.setText(corrected_text)
			except:
				pass

	# CREATE TASK CLICKED
	def button_create_clicked(self):
		# SANITY CHECKS
		invalid_widgets = self.sanity_checks()
		# VALID
		if len(invalid_widgets) == 0:
			file_name = self.file_name_line.text()
			# GET MODE
			mode = 'empty_file'
			if self.from_current_file_radio.isChecked():
				mode = 'from_current_file'

			full_name = self.create_full_filename(file_name)

			# CREATE FILE
			try:
				new_file_fragments = [self.root_dir, full_name]
				new_file = os.path.join(*new_file_fragments)
				new_file = Path(new_file).as_posix()
				if mode == 'empty_file':
					source_file = os.path.join(spcvars.pipeline_root, '_sourcefiles/houdini/empty.hip')
					source_file = Path(source_file).as_posix()
					shutil.copyfile(source_file, new_file)
					self.the_signal.emit(new_file)
					self.close()
				elif mode == 'from_current_file':
					hou.hipFile.save(file_name=new_file)
					self.the_signal.emit(new_file)
					self.close()
				# WRITE RECENT FILE INFO
				recent_dir = user_temp_dir
				spc_recent_info.write(recent_dir, new_file)

			except:
				print('SOMETHING WENT WRONG')
		# INVALID
		else:
			for i in invalid_widgets:
				i.setStyleSheet("color:#c8290b")

	# SANITY CHECKS
	def sanity_checks(self):
		invalid_widgets = []
		# GATHER VARIABLES
		file_name = self.file_name_line.text()

		# CHECK IF TASK NAME ALREADY EXISTS
		if file_name in self.existing_files:
			invalid_widgets.append(self.file_name_label)
			invalid_widgets.append(self.file_name_line)
			hou.ui.displayMessage('File "{}" already exists!'.format(file_name), title='ERROR', severity=hou.severityType.Error)

		# NO NAME GIVEN
		elif not file_name:
			invalid_widgets.append(self.file_name_label)
			invalid_widgets.append(self.file_name_line)
			hou.ui.displayMessage('Please enter a Name!', title='ERROR', severity=hou.severityType.Error)

		# CHECK FOR INVALID CHARACTERS
		for i in file_name:
			if not i.isalpha():
				invalid_widgets.append(self.file_name_label)
				invalid_widgets.append(self.file_name_line)
				hou.ui.displayMessage('Character "{}" not allowed!'.format(i), title='ERROR', severity=hou.severityType.Error)
				break


		# RETURN
		return invalid_widgets

	# CREATE FULL FILENAME
	def create_full_filename(self, file_name):
		structure_dict = spc_structurize.structurize_path(self.root_dir)
		prefix = structure_dict.get('project_prefix')
		name = structure_dict.get('name')
		task = structure_dict.get('task')
		full_name = '_'.join([prefix, name, task, file_name, 'v001.hip'])

		return full_name


# NEW TASK WINDOW
class NewTaskWindow(pipeline_window):
	the_signal = Signal(str)

	def __init__(self, root_dir, existing_tasks=None):
		super(NewTaskWindow, self).__init__()
		self.root_dir = root_dir
		self.existing_tasks = existing_tasks
		# MAIN WINDOW SETTINGS
		self.setWindowTitle('Create New Task')
		self.resize(300, 100)
		hou_stylesheet = hou.qt.styleSheet()
		self.setStyleSheet(hou_stylesheet)

		self.edit_ui()

	# EDIT UI
	def edit_ui(self):
		# OVERWRITE EXISTING WIDGETS
		self.main_group.setTitle('Settings')
		self.button_ok.setText('Create')
		self.button_ok.clicked.connect(self.button_create_clicked)
		self.setWindowIcon(QIcon(os.path.join(icons_dir, 'add_dark.png')))

		label_width = 65

		# TASK NAME
		task_name_hori = QHBoxLayout()
		self.main_layout.addLayout(task_name_hori)
		self.task_name_label = QLabel('Name:')
		task_name_tooltip = 'Name can be a mix of uppercase alphabetical letters \nwith "_" being allowed.'
		self.task_name_label.setToolTip(task_name_tooltip)
		self.task_name_label.setFixedWidth(label_width)
		self.task_name_line = QLineEdit()
		self.task_name_line.setToolTip(task_name_tooltip)
		self.task_name_line.textEdited.connect(lambda: reset_stylesheet([self.task_name_line, self.task_name_label]))
		self.task_name_line.textEdited.connect(lambda: self.conform_user_input('task_name'))
		task_name_hori.addWidget(self.task_name_label)
		task_name_hori.addWidget(self.task_name_line)

		# OPEN
		self.show()

	# CONFORM USER INPUT
	def conform_user_input(self, option):
		if option == 'task_name':
			try:
				user_text = self.task_name_line.text()
				corrected_text = user_text
				# FORCE UPPERCASE
				corrected_text = user_text.upper()
				# REPLACE SPACES
				corrected_text = corrected_text.replace(' ', '_')
				# SET
				self.task_name_line.setText(corrected_text)
			except:
				pass

	# CREATE TASK CLICKED
	def button_create_clicked(self):
		# SANITY CHECKS
		invalid_widgets = self.sanity_checks()
		if len(invalid_widgets) == 0:
			task_name = self.task_name_line.text()
			# VALID
			# CREATE TASK
			try:
				creation = spc_create_dirs.create_task(self.root_dir, task_name)
				message = list(creation.values())[0]
				if 'success' in creation:
					self.the_signal.emit(task_name)
					self.close()
					hou.ui.displayMessage(message, title='YEY!', severity=hou.severityType.ImportantMessage)
				elif 'exception' in creation:
					self.the_signal.emit(None)
					self.close()
					hou.ui.displayMessage('Something went wrong. Please ask Pipeline Dude!', title='ERROR', severity=hou.severityType.Error, details=message)
				else:
					self.close()
					hou.ui.displayMessage(message, title='ERROR', severity=hou.severityType.Error)
			except Exception as error_message:
				self.the_signal.emit(None)
				self.close()
				err_string = repr(error_message)
				hou.ui.displayMessage('Could not create Task. Please ask Pipeline Dude!', title='ERROR',
									  severity=hou.severityType.Error, details=err_string)
		else:
			# INVALID
			for i in invalid_widgets:
				i.setStyleSheet("color:#c8290b")

	# SANITY CHECKS
	def sanity_checks(self):
		invalid_widgets = []
		# GATHER VARIABLES
		task_name = self.task_name_line.text()

		# CHECK IF TASK NAME ALREADY EXISTS
		if task_name in self.existing_tasks:
			invalid_widgets.append(self.task_name_label)
			invalid_widgets.append(self.task_name_line)
			hou.ui.displayMessage('Task "{}" already exists!'.format(task_name), title='ERROR', severity=hou.severityType.Error)

		# NO NAME GIVEN
		elif not task_name:
			invalid_widgets.append(self.task_name_label)
			invalid_widgets.append(self.task_name_line)
			hou.ui.displayMessage('Please enter a Name!', title='ERROR', severity=hou.severityType.Error)

		# RETURN
		return invalid_widgets


# NEW RND WINDOW
class NewRnDWindow(pipeline_window):
	the_signal = Signal(str)

	def __init__(self, project_root, existing_rnds=None):
		super(NewRnDWindow, self).__init__()
		self.project_root = project_root
		self.existing_rnds = existing_rnds
		# MAIN WINDOW SETTINGS
		self.setWindowTitle('Create New RnD File')
		self.resize(350, 150)
		hou_stylesheet = hou.qt.styleSheet()
		self.setStyleSheet(hou_stylesheet)

		self.edit_ui()

	# EDIT UI
	def edit_ui(self):
		# OVERWRITE EXISTING WIDGETS
		self.main_group.setTitle('Settings')
		self.button_ok.setText('Create')
		self.button_ok.clicked.connect(self.button_create_clicked)
		self.setWindowIcon(QIcon(os.path.join(icons_dir, 'add_dark.png')))

		label_width = 80

		# RND NAME
		rnd_name_hori = QHBoxLayout()
		self.main_layout.addLayout(rnd_name_hori)
		self.rnd_name_label = QLabel('Name:')
		rnd_name_tooltip = 'Name can be a mix of uppercase alphabetical and numerical letters\nwith "_" or "-" being allowed.'
		self.rnd_name_label.setToolTip(rnd_name_tooltip)
		self.rnd_name_label.setFixedWidth(label_width)
		self.rnd_name_line = QLineEdit()
		self.rnd_name_line.setToolTip(rnd_name_tooltip)
		self.rnd_name_line.textEdited.connect(lambda: reset_stylesheet([self.rnd_name_line, self.rnd_name_label]))
		self.rnd_name_line.textEdited.connect(lambda: self.conform_user_input('rnd_name'))
		rnd_name_hori.addWidget(self.rnd_name_label)
		rnd_name_hori.addWidget(self.rnd_name_line)

		# TIMELINE
		timeline_hori = QHBoxLayout()
		self.main_layout.addLayout(timeline_hori)
		self.timeline_label = QLabel('Timeline:')
		self.timeline_label.setFixedWidth(label_width)
		self.from_line = QLineEdit('1001')
		self.from_line.setFixedWidth(60)
		self.from_line.textEdited.connect(lambda: reset_stylesheet([self.from_line]))
		self.to_label = QLabel(' - ')
		self.to_line = QLineEdit('1101')
		self.to_line.setFixedWidth(60)
		self.to_line.textEdited.connect(lambda: reset_stylesheet([self.end_line, self.end_label]))
		timeline_hori.addWidget(self.timeline_label)
		timeline_hori.addWidget(self.from_line)
		timeline_hori.addWidget(self.to_label)
		timeline_hori.addWidget(self.to_line)
		timeline_hori.addStretch()

		# RESOLUTION
		# SET PROJECT RESOLUTION AS DEFAULT VALUES
		project_res_w = spcsettings.get_project_setting(self.project_root, 'project_res_width')
		project_res_h = spcsettings.get_project_setting(self.project_root, 'project_res_height')
		res_hori = QHBoxLayout()
		self.main_layout.addLayout(res_hori)
		self.res_label = QLabel('Resolution:')
		self.res_label.setFixedWidth(label_width)
		self.res_width_line = QLineEdit(project_res_w)
		self.res_width_line.textEdited.connect(lambda: reset_stylesheet([self.res_width_line, self.res_label]))
		self.res_width_line.setToolTip('Width')
		self.res_width_line.setFixedWidth(60)
		res_x_label = QLabel(' x ')
		res_x_label.setFixedWidth(10)
		self.res_height_line = QLineEdit(project_res_h)
		self.res_height_line.textEdited.connect(lambda: reset_stylesheet([self.res_height_line, self.res_label]))
		self.res_height_line.setToolTip('Height')
		self.res_height_line.setFixedWidth(60)
		res_hori.addWidget(self.res_label)
		res_hori.addWidget(self.res_width_line)
		res_hori.addWidget(res_x_label)
		res_hori.addWidget(self.res_height_line)
		res_hori.addStretch()

		# OPEN
		self.show()

	# CONFORM USER INPUT
	def conform_user_input(self, option):
		if option == 'rnd_name':
			try:
				user_text = self.rnd_name_line.text()
				corrected_text = user_text
				# FORCE UPPERCASE
				corrected_text = user_text.upper()
				# REPLACE SPACES
				corrected_text = corrected_text.replace(' ', '-')
				# SET
				self.rnd_name_line.setText(corrected_text)
			except:
				pass

	# CREATE RND CLICKED
	def button_create_clicked(self):
		# SANITY CHECKS
		invalid_widgets = self.sanity_checks()
		if len(invalid_widgets) == 0:
			rnd_name = self.rnd_name_line.text()
			# SETTINGS
			settings_dict = {}
			settings_dict['res_width'] = self.res_width_line.text()
			settings_dict['res_height'] = self.res_height_line.text()
			settings_dict['range_from'] = self.from_line.text()
			settings_dict['range_to'] = self.to_line.text()
			# VALID
			# CREATE RND
			try:
				creation = spc_create_dirs.create_rnd(self.project_root, rnd_name, settings_dict=settings_dict)
				message = list(creation.values())[0]
				if 'success' in creation:
					self.the_signal.emit(rnd_name)
					self.close()
					hou.ui.displayMessage(message, title='YEY!', severity=hou.severityType.ImportantMessage)
				elif 'exception' in creation:
					self.the_signal.emit(None)
					self.close()
					hou.ui.displayMessage('Something went wrong. Please ask Pipeline Dude!', title='ERROR', severity=hou.severityType.Error, details=message)
				else:
					self.close()
					hou.ui.displayMessage(message, title='ERROR', severity=hou.severityType.Error)
			except Exception as error_message:
				self.the_signal.emit(None)
				self.close()
				err_string = repr(error_message)
				hou.ui.displayMessage('Could not create Rnd. Please ask Pipeline Dude!', title='ERROR',
									  severity=hou.severityType.Error, details=err_string)
		else:
			# INVALID
			for i in invalid_widgets:
				i.setStyleSheet("color:#c8290b")

	# SANITY CHECKS
	def sanity_checks(self):
		invalid_widgets = []
		# GATHER VARIABLES
		rnd_name = self.rnd_name_line.text()
		from_frame = self.from_line.text()
		to_frame = self.to_line.text()
		res_w = self.res_width_line.text()
		res_h = self.res_height_line.text()

		# NAME ALREADY EXISTS
		if rnd_name in self.existing_rnds:
			invalid_widgets.append(self.rnd_name_line)
			invalid_widgets.append(self.rnd_name_label)
			hou.ui.displayMessage('Rnd "{}" already exists!'.format(rnd_name), title='ERROR', severity=hou.severityType.Error)
		# NO NAME GIVEN
		elif not rnd_name:
			invalid_widgets.append(self.rnd_name_line)
			invalid_widgets.append(self.rnd_name_label)
			hou.ui.displayMessage('Please enter a Name!', title='ERROR', severity=hou.severityType.Error)

		# CHECK IF DIGITS
		# TO DO

		# RETURN
		return invalid_widgets


# NEW ASSET WINDOW
class NewAssetWindow(pipeline_window):
	the_signal = Signal(str)

	def __init__(self, project_root, existing_assets=None):
		super(NewAssetWindow, self).__init__()
		self.project_root = project_root
		self.existing_assets = existing_assets
		# MAIN WINDOW SETTINGS
		self.setWindowTitle('Create New Asset')
		self.resize(350, 150)
		hou_stylesheet = hou.qt.styleSheet()
		self.setStyleSheet(hou_stylesheet)

		self.edit_ui()

	# EDIT UI
	def edit_ui(self):
		# OVERWRITE EXISTING WIDGETS
		self.main_group.setTitle('Settings')
		self.button_ok.setText('Create')
		self.button_ok.clicked.connect(self.button_create_clicked)
		self.setWindowIcon(QIcon(os.path.join(icons_dir, 'add_dark.png')))

		label_width = 80

		# ASSET NAME
		asset_name_hori = QHBoxLayout()
		self.main_layout.addLayout(asset_name_hori)
		self.asset_name_label = QLabel('Name:')
		asset_name_tooltip = 'Name can be a mix of uppercase alphabetical and numerical letters\nwith "_" or "-" being allowed.'
		self.asset_name_label.setToolTip(asset_name_tooltip)
		self.asset_name_label.setFixedWidth(label_width)
		self.asset_name_line = QLineEdit()
		self.asset_name_line.setToolTip(asset_name_tooltip)
		self.asset_name_line.textEdited.connect(lambda: reset_stylesheet([self.asset_name_line, self.asset_name_label]))
		self.asset_name_line.textEdited.connect(lambda: self.conform_user_input('asset_name'))
		asset_name_hori.addWidget(self.asset_name_label)
		asset_name_hori.addWidget(self.asset_name_line)

		# TIMELINE
		timeline_hori = QHBoxLayout()
		self.main_layout.addLayout(timeline_hori)
		self.timeline_label = QLabel('Timeline:')
		self.timeline_label.setFixedWidth(label_width)
		self.from_line = QLineEdit('1001')
		self.from_line.setFixedWidth(60)
		self.from_line.textEdited.connect(lambda: reset_stylesheet([self.from_line]))
		self.to_label = QLabel(' - ')
		self.to_line = QLineEdit('1101')
		self.to_line.setFixedWidth(60)
		self.to_line.textEdited.connect(lambda: reset_stylesheet([self.end_line, self.end_label]))
		timeline_hori.addWidget(self.timeline_label)
		timeline_hori.addWidget(self.from_line)
		timeline_hori.addWidget(self.to_label)
		timeline_hori.addWidget(self.to_line)
		timeline_hori.addStretch()

		# RESOLUTION
		# SET PROJECT RESOLUTION AS DEFAULT VALUES
		project_res_w = spcsettings.get_project_setting(self.project_root, 'project_res_width')
		project_res_h = spcsettings.get_project_setting(self.project_root, 'project_res_height')
		res_hori = QHBoxLayout()
		self.main_layout.addLayout(res_hori)
		self.res_label = QLabel('Resolution:')
		self.res_label.setFixedWidth(label_width)
		self.res_width_line = QLineEdit(project_res_w)
		self.res_width_line.textEdited.connect(lambda: reset_stylesheet([self.res_width_line, self.res_label]))
		self.res_width_line.setToolTip('Width')
		self.res_width_line.setFixedWidth(60)
		res_x_label = QLabel(' x ')
		res_x_label.setFixedWidth(10)
		self.res_height_line = QLineEdit(project_res_h)
		self.res_height_line.textEdited.connect(lambda: reset_stylesheet([self.res_height_line, self.res_label]))
		self.res_height_line.setToolTip('Height')
		self.res_height_line.setFixedWidth(60)
		res_hori.addWidget(self.res_label)
		res_hori.addWidget(self.res_width_line)
		res_hori.addWidget(res_x_label)
		res_hori.addWidget(self.res_height_line)
		res_hori.addStretch()

		# OPEN
		self.show()

	# CONFORM USER INPUT
	def conform_user_input(self, option):
		if option == 'asset_name':
			try:
				user_text = self.asset_name_line.text()
				corrected_text = user_text
				# FORCE UPPERCASE
				corrected_text = user_text.upper()
				# REPLACE SPACES
				corrected_text = corrected_text.replace(' ', '-')
				# SET
				self.asset_name_line.setText(corrected_text)
			except:
				pass

	# CREATE ASSET CLICKED
	def button_create_clicked(self):
		# SANITY CHECKS
		invalid_widgets = self.sanity_checks()
		if len(invalid_widgets) == 0:
			asset_name = self.asset_name_line.text()
			# SETTINGS
			settings_dict = {}
			settings_dict['res_width'] = self.res_width_line.text()
			settings_dict['res_height'] = self.res_height_line.text()
			settings_dict['range_from'] = self.from_line.text()
			settings_dict['range_to'] = self.to_line.text()
			# VALID
			# CREATE ASSET
			try:
				creation = spc_create_dirs.create_asset(self.project_root, asset_name, settings_dict=settings_dict)
				message = list(creation.values())[0]
				if 'success' in creation:
					self.the_signal.emit(asset_name)
					self.close()
					hou.ui.displayMessage(message, title='YEY!', severity=hou.severityType.ImportantMessage)
				elif 'exception' in creation:
					self.the_signal.emit(None)
					self.close()
					hou.ui.displayMessage('Something went wrong. Please ask Pipeline Dude!', title='ERROR', severity=hou.severityType.Error, details=message)
				else:
					self.close()
					hou.ui.displayMessage(message, title='ERROR', severity=hou.severityType.Error)
			except Exception as error_message:
				self.the_signal.emit(None)
				self.close()
				err_string = repr(error_message)
				hou.ui.displayMessage('Could not create Asset. Please ask Pipeline Dude!', title='ERROR',
									  severity=hou.severityType.Error, details=err_string)
		else:
			# INVALID
			for i in invalid_widgets:
				i.setStyleSheet("color:#c8290b")

	# SANITY CHECKS
	def sanity_checks(self):
		invalid_widgets = []
		# GATHER VARIABLES
		asset_name = self.asset_name_line.text()
		from_frame = self.from_line.text()
		to_frame = self.to_line.text()
		res_w = self.res_width_line.text()
		res_h = self.res_height_line.text()

		# NAME ALREADY EXISTS
		if asset_name in self.existing_assets:
			invalid_widgets.append(self.asset_name_line)
			invalid_widgets.append(self.asset_name_label)
			hou.ui.displayMessage('Asset "{}" already exists!'.format(asset_name), title='ERROR', severity=hou.severityType.Error)
		# NO NAME GIVEN
		elif not asset_name:
			invalid_widgets.append(self.asset_name_line)
			invalid_widgets.append(self.asset_name_label)
			hou.ui.displayMessage('Please enter a Name!', title='ERROR', severity=hou.severityType.Error)

		# CHECK IF DIGITS
		# TO DO

		# RETURN
		return invalid_widgets


# NEW SHOT WINDOW
class NewShotWindow(pipeline_window):
	the_signal = Signal(str)

	def __init__(self, project_root, existing_shots=None):
		super(NewShotWindow, self).__init__()
		self.project_root = project_root
		self.existing_shots = existing_shots
		# MAIN WINDOW SETTINGS
		self.setWindowTitle('Create New Shot')
		self.resize(350, 150)
		hou_stylesheet = hou.qt.styleSheet()
		self.setStyleSheet(hou_stylesheet)

		self.edit_ui()

	# EDIT UI
	def edit_ui(self):
		# OVERWRITE EXISTING WIDGETS
		self.main_group.setTitle('Settings')
		self.button_ok.setText('Create')
		self.button_ok.clicked.connect(self.button_create_clicked)
		self.setWindowIcon(QIcon(os.path.join(icons_dir, 'add_dark.png')))

		label_width = 80
		
		# SHOT NAME
		shot_name_hori = QHBoxLayout()
		self.main_layout.addLayout(shot_name_hori)
		self.shot_name_label = QLabel('Name:')
		shot_name_tooltip = 'Name can be a mix of uppercase alphabetical and numerical letters\nwith "_" or "-" being allowed.'
		self.shot_name_label.setToolTip(shot_name_tooltip)
		self.shot_name_label.setFixedWidth(label_width)
		self.shot_name_line = QLineEdit()
		self.shot_name_line.setToolTip(shot_name_tooltip)
		self.shot_name_line.textEdited.connect(lambda: reset_stylesheet([self.shot_name_line, self.shot_name_label]))
		self.shot_name_line.textEdited.connect(lambda: self.conform_user_input('shot_name'))
		shot_name_hori.addWidget(self.shot_name_label)
		shot_name_hori.addWidget(self.shot_name_line)

		# TIMELINE
		timeline_hori = QHBoxLayout()
		self.main_layout.addLayout(timeline_hori)
		self.timeline_label = QLabel('Timeline:')
		self.timeline_label.setFixedWidth(label_width)
		self.from_line = QLineEdit('1001')
		self.from_line.setFixedWidth(60)
		self.from_line.textEdited.connect(lambda: reset_stylesheet([self.from_line]))
		self.to_label = QLabel(' - ')
		self.to_line = QLineEdit('1101')
		self.to_line.setFixedWidth(60)
		self.to_line.textEdited.connect(lambda: reset_stylesheet([self.end_line, self.end_label]))
		timeline_hori.addWidget(self.timeline_label)
		timeline_hori.addWidget(self.from_line)
		timeline_hori.addWidget(self.to_label)
		timeline_hori.addWidget(self.to_line)
		timeline_hori.addStretch()

		# RESOLUTION
		# SET PROJECT RESOLUTION AS DEFAULT VALUES
		project_res_w = spcsettings.get_project_setting(self.project_root, 'project_res_width')
		project_res_h = spcsettings.get_project_setting(self.project_root, 'project_res_height')
		res_hori = QHBoxLayout()
		self.main_layout.addLayout(res_hori)
		self.res_label = QLabel('Resolution:')
		self.res_label.setFixedWidth(label_width)
		self.res_width_line = QLineEdit(project_res_w)
		self.res_width_line.textEdited.connect(lambda: reset_stylesheet([self.res_width_line, self.res_label]))
		self.res_width_line.setToolTip('Width')
		self.res_width_line.setFixedWidth(60)
		res_x_label = QLabel(' x ')
		res_x_label.setFixedWidth(10)
		self.res_height_line = QLineEdit(project_res_h)
		self.res_height_line.textEdited.connect(lambda: reset_stylesheet([self.res_height_line, self.res_label]))
		self.res_height_line.setToolTip('Height')
		self.res_height_line.setFixedWidth(60)
		res_hori.addWidget(self.res_label)
		res_hori.addWidget(self.res_width_line)
		res_hori.addWidget(res_x_label)
		res_hori.addWidget(self.res_height_line)
		res_hori.addStretch()

		# OPEN
		self.show()

	# CONFORM USER INPUT
	def conform_user_input(self, option):
		if option == 'shot_name':
			try:
				user_text = self.shot_name_line.text()
				corrected_text = user_text
				# FORCE UPPERCASE
				corrected_text = user_text.upper()
				# REPLACE SPACES
				corrected_text = corrected_text.replace(' ', '-')
				# SET
				self.shot_name_line.setText(corrected_text)
			except:
				pass

	# CREATE SHOT CLICKED
	def button_create_clicked(self):
		# SANITY CHECKS
		invalid_widgets = self.sanity_checks()
		if len(invalid_widgets) == 0:
			shot_name = self.shot_name_line.text()
			# SETTINGS
			settings_dict = {}
			settings_dict['res_width'] = self.res_width_line.text()
			settings_dict['res_height'] = self.res_height_line.text()
			settings_dict['range_from'] = self.from_line.text()
			settings_dict['range_to'] = self.to_line.text()
			# VALID
			# CREATE SHOT
			try:
				creation = spc_create_dirs.create_shot(self.project_root, shot_name, settings_dict=settings_dict)
				message = list(creation.values())[0]
				if 'success' in creation:
					self.the_signal.emit(shot_name)
					self.close()
					hou.ui.displayMessage(message, title='YEY!', severity=hou.severityType.ImportantMessage)
				elif 'exception' in creation:
					self.the_signal.emit(None)
					self.close()
					hou.ui.displayMessage('Something went wrong. Please ask Pipeline Dude!', title='ERROR', severity=hou.severityType.Error, details=message)
				else:
					self.close()
					hou.ui.displayMessage(message, title='ERROR', severity=hou.severityType.Error)
			except Exception as error_message:
				self.the_signal.emit(None)
				self.close()
				err_string = repr(error_message)
				hou.ui.displayMessage('Could not create Shot. Please ask Pipeline Dude!', title='ERROR', severity=hou.severityType.Error, details=err_string)
		else:
			# INVALID
			for i in invalid_widgets:
				i.setStyleSheet("color:#c8290b")

	# SANITY CHECKS
	def sanity_checks(self):
		invalid_widgets = []
		# GATHER VARIABLES
		shot_name = self.shot_name_line.text()
		from_frame = self.from_line.text()
		to_frame = self.to_line.text()
		res_w = self.res_width_line.text()
		res_h = self.res_height_line.text()

		# NAME ALREADY EXISTS
		if shot_name in self.existing_shots:
			invalid_widgets.append(self.shot_name_line)
			invalid_widgets.append(self.shot_name_label)
			hou.ui.displayMessage('Shot "{}" already exists!'.format(shot_name), title='ERROR', severity=hou.severityType.Error)
		# NO NAME GIVEN
		elif not shot_name:
			invalid_widgets.append(self.shot_name_line)
			invalid_widgets.append(self.shot_name_label)
			hou.ui.displayMessage('Please enter a Name!', title='ERROR', severity=hou.severityType.Error)

		# CHECK IF DIGITS
		# TO DO

		# RETURN
		return invalid_widgets


# NEW PROJECT WINDOW
class NewProjectWindow(pipeline_window):
	the_signal = Signal(str)

	def __init__(self):
		super(NewProjectWindow, self).__init__()
		importlib.reload(spcvars)
		importlib.reload(spc_get_dir)
		importlib.reload(spc_create_project)
		# MAIN WINDOW SETTINGS
		self.setWindowTitle('Create New Project')
		self.resize(400, 230)
		hou_stylesheet = hou.qt.styleSheet()
		self.setStyleSheet(hou_stylesheet)

		self.edit_ui()

	# EDIT UI
	def edit_ui(self):
		# OVERWRITE EXISTING WIDGETS
		self.main_group.setTitle('Settings')
		self.button_ok.setText('Create')
		self.button_ok.clicked.connect(self.button_create_clicked)
		self.setWindowIcon(QIcon(os.path.join(icons_dir, 'add_dark.png')))

		label_width = 65

		# ROOT
		project_root_hori = QHBoxLayout()
		self.main_layout.addLayout(project_root_hori)
		self.project_root_label = QLabel('Root:')
		self.project_root_label.setFixedWidth(label_width)
		self.project_root_line = QLineEdit()
		self.project_root_line.setText(spcvars.global_project_root)
		self.project_root_line.setEnabled(False)
		project_root_hori.addWidget(self.project_root_label )
		project_root_hori.addWidget(self.project_root_line)

		# PROJECT NAME
		project_name_hori = QHBoxLayout()
		self.main_layout.addLayout(project_name_hori)
		self.project_name_label = QLabel('Name:')
		project_name_tooltip = 'Project Name has to be alphabetical lowercase letters'
		self.project_name_label.setToolTip(project_name_tooltip)
		self.project_name_label.setFixedWidth(label_width)
		self.project_name_line = QLineEdit()
		self.project_name_line.textEdited.connect(lambda: reset_stylesheet([self.project_name_line, self.project_name_label]))
		self.project_name_line.textEdited.connect(lambda: self.conform_user_input('project_name'))
		self.project_name_line.setToolTip(project_name_tooltip)
		self.project_name_line.setFocus()
		project_name_hori.addWidget(self.project_name_label)
		project_name_hori.addWidget(self.project_name_line)

		# PROJECT PREFIX
		project_prefix_hori = QHBoxLayout()
		self.main_layout.addLayout(project_prefix_hori)
		self.project_prefix_label = QLabel('Prefix:')
		self.project_prefix_label.setFixedWidth(label_width)
		project_prefix_tooltip = 'Project Prefix has to be 4 alphabetical uppercase Letters'
		self.project_prefix_label.setToolTip(project_prefix_tooltip)
		self.project_prefix_line = QLineEdit()
		self.project_prefix_line.textEdited.connect(lambda: reset_stylesheet([self.project_prefix_line, self.project_prefix_label]))
		self.project_prefix_line.textEdited.connect(lambda: self.conform_user_input('project_prefix'))
		self.project_prefix_line.setMaxLength(4)
		self.project_prefix_line.setFixedWidth(label_width)
		self.project_prefix_line.setToolTip(project_prefix_tooltip)
		project_prefix_hori.addWidget(self.project_prefix_label)
		project_prefix_hori.addWidget(self.project_prefix_line)
		project_prefix_hori.addStretch()

		# PROJECT RESOLUTION
		project_res_hori = QHBoxLayout()
		self.main_layout.addLayout(project_res_hori)
		self.project_res_label = QLabel('Resolution:')
		self.project_res_label.setFixedWidth(label_width)
		self.project_res_width_line = QLineEdit('1920')
		self.project_res_width_line.textEdited.connect(lambda: reset_stylesheet([self.project_res_width_line, self.project_res_label]))
		self.project_res_width_line.setToolTip('Width')
		self.project_res_width_line.setFixedWidth(60)
		project_res_x_label = QLabel(' x ')
		project_res_x_label.setFixedWidth(10)
		self.project_res_height_line = QLineEdit('1080')
		self.project_res_height_line.textEdited.connect(lambda: reset_stylesheet([self.project_res_height_line, self.project_res_label]))
		self.project_res_height_line.setToolTip('Height')
		self.project_res_height_line.setFixedWidth(60)
		project_res_hori.addWidget(self.project_res_label)
		project_res_hori.addWidget(self.project_res_width_line)
		project_res_hori.addWidget(project_res_x_label)
		project_res_hori.addWidget(self.project_res_height_line)
		project_res_hori.addStretch()

		# PROJECT FRAMERATE
		project_fps_hori = QHBoxLayout()
		self.main_layout.addLayout(project_fps_hori)
		self.project_fps_label = QLabel('Framerate:')
		self.project_fps_label.setFixedWidth(label_width)
		self.project_fps_line = QLineEdit('24')
		self.project_fps_line.textEdited.connect(lambda: reset_stylesheet([self.project_fps_line, self.project_fps_label]))
		self.project_fps_line.setFixedWidth(60)
		project_fps_hori.addWidget(self.project_fps_label)
		project_fps_hori.addWidget(self.project_fps_line)
		project_fps_hori.addStretch()

		# OPEN
		self.show()

	# CREATE PROJECT CLICKED
	def button_create_clicked(self):
		# SANITY CHECKS
		invalid_widgets = self.sanity_checks()
		if len(invalid_widgets) == 0:
			# VALID
			# CREATE PROJECT
			project_root = self.project_root_line.text()
			project_name = self.project_name_line.text()
			project_prefix = self.project_prefix_line.text()
			project_res_w = self.project_res_width_line.text()
			project_res_h = self.project_res_height_line.text()
			project_fps = self.project_fps_line.text()

			creation = spc_create_project.create_project(project_root, project_name, project_prefix, project_res_w, project_res_h, project_fps)
			message = list(creation.values())[0]
			if 'success' in creation:
				self.the_signal.emit(project_name)
				self.close()
				hou.ui.displayMessage(message, title='YEY!', severity=hou.severityType.ImportantMessage)
			elif 'exception' in creation:
				self.close()
				hou.ui.displayMessage('Something went wrong. Please ask Pipeline Dude!', title='ERROR', severity=hou.severityType.Error, details=message)
			else:
				self.close()
				hou.ui.displayMessage(message, title='ERROR', severity=hou.severityType.Error)

			'''
			except Exception as error_message:
				self.close()
				err_string = repr(error_message)
				hou.ui.displayMessage('Could not create Project. Please ask Pipeline Dude!', title='ERROR',
									  severity=hou.severityType.Error, details=err_string)
		  	'''
		else:
			# INVALID
			for i in invalid_widgets:
				i.setStyleSheet("color:#c8290b")

	# CONFORM USER INPUT
	def conform_user_input(self, option):
		if option == 'project_name':
			try:
				user_text = self.project_name_line.text()
				self.project_name_line.setText(user_text.lower())
			except:
				pass
		elif option == 'project_prefix':
			try:
				user_text = self.project_prefix_line.text()
				self.project_prefix_line.setText(user_text.upper())
			except:
				pass

	# SANITY CHECKS
	def sanity_checks(self):
		invalid_widgets = []
		# GATHER VARIABLES
		project_root = self.project_root_line.text()
		project_name = self.project_name_line.text().lower()
		project_prefix = self.project_prefix_line.text().upper()
		project_res_w = self.project_res_width_line.text()
		proejct_res_h = self.project_res_height_line.text()

		# CHECK IF PROJECT ROOT EXISTS
		if not os.path.isdir(project_root):
			invalid_widgets.append(self.project_root_line)

		# CHECK PROJECT NAME
		if not len(project_name) > 3:
			invalid_widgets.append(self.project_name_line)
			invalid_widgets.append(self.project_name_label)
		else:
			for char in project_name:
				if not char.isalpha():
					invalid_widgets.append(self.project_name_line)
					invalid_widgets.append(self.project_name_label)
					break

		# CHECK PROJECT PREFIX
		if len(project_prefix) < 4:
			invalid_widgets.append(self.project_prefix_line)
			invalid_widgets.append(self.project_prefix_label)

		# RETURN
		return invalid_widgets


# RESET STYLE SHEETS
def reset_stylesheet(widgets):
	hou_stylesheet = hou.qt.styleSheet()
	for widget in widgets:
		widget.setStyleSheet(hou_stylesheet)


# DELETE WINDOW IF ALREADY EXISTING
for entry in QApplication.allWidgets():
	if type(entry).__name__ == 'MissionCtrlWindow':
		entry.close()

# VARIABLE TO CALL FROM SHELF TOOL
window = MissionCtrlWindow()

import hou

def create_renderobjects_from_selection():
    # GET SELECTION
    selected_nodes = hou.selectedNodes()
    if selected_nodes:

        # CHECK IF SELECTION CONTAINS OF SOP NODES
        all_sop = True
        for i in selected_nodes:
            node_category = i.type().category().name()
            if node_category != 'Sop':
                all_sop = False

        # SELECTION ONLY CONTAINS OF SOP NODES
        if all_sop:
            # FIND THE NEXT OBJ LEVEL IN PARENT HIERARCHY TO PASTE THE RENDER GEO TO
            obj_network = find_parent_object_net(selected_nodes[0])
            #obj_network = hou.node('/obj/')
            new_renderobjects = []
            init_node_placement = [3.5, -13]
            strip_strings = ['OUT_', 'TO_RENDER_', 'RENDER_']


            for index, selected_node in enumerate(selected_nodes):
                selected_node_name = selected_node.name()
                # MODIFY NAME IF ANY STRIP STRING WAS FOUND
                for strip_string in strip_strings:
                    if selected_node_name.startswith(strip_string):
                        selected_node_name = selected_node_name.replace(strip_string, '')
                # ADD RENDER_ PREFIX TO NODE NAME
                selected_node_name = 'RENDER_{}'.format(selected_node_name)

                # CREATE NEW GEO NODE ON /obj/ LEVEL
                render_obj_node = obj_network.createNode('geo', node_name=selected_node_name)
                render_obj_node.setColor(hou.Color((0,0,0)))
                render_obj_node.setDisplayFlag(0)

                # DIVE INTO NEW GEO NODE AND CREATE SOP NODES
                object_merge_node = render_obj_node.createNode('object_merge', node_name='FETCH_INPUT')
                rel_selected_node_path = object_merge_node.relativePathTo(selected_node)
                object_merge_node.parm('objpath1').set(rel_selected_node_path)
                to_render_null_node = render_obj_node.createNode('null', node_name='TO_RENDER')
                to_render_null_node.setFirstInput(object_merge_node)
                to_render_null_node.setColor(hou.Color((0,0,0)))
                to_render_null_node.setRenderFlag(1)
                to_render_null_node.setDisplayFlag(1)
                to_render_null_node.setTemplateFlag(1)
                render_obj_node.layoutChildren()

                # SORT NEWLY CREATED NODES ON OBJ LEVEL
                # GENERATE NEW NODE POSITION IN NETWORK
                node_pos = [init_node_placement[0], init_node_placement[1] + index]
                render_obj_node.setPosition(node_pos)

                # APPEND TO NEW RENDEROBJECTS LIST
                new_renderobjects.append(render_obj_node)

            # JUMP TO NEWLY CREATED NODES
            for i in new_renderobjects:
                i.setSelected(True)
                p = hou.ui.paneTabOfType(hou.paneTabType.NetworkEditor)
                p.setCurrentNode(i)
                p.homeToSelection()

        # SELECTION DOESNT ONLY CONTAIN SOP NODES
        else:
            hou.ui.displayMessage('Please select SOP Node(s) to create Renderobject(s) out of.', title='Warning',
                                  severity=hou.severityType.Warning)
    # NOTHING SELECTED
    else:
        hou.ui.displayMessage('Please select SOP Node(s) to create Renderobject(s) out of.', title='Warning',
                              severity=hou.severityType.Warning)


def find_parent_object_net(node):
    # NODE IS AN OBJ NODE SO RETURN ITS PARENT (WHICH IS THE OBJ NETWORK)
    if isinstance(node, hou.ObjNode):
        return node.parent()
    # GET PARENT
    parent = node.parent()
    # IF THERE IS NO PARENT, ITS FAILED
    if not parent:
        return None
    # CHECK PARENT
    return find_parent_object_net(parent)
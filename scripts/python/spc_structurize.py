import os
import spc_global_pipeline_vars as spcvars
import spc_read_settings as spcsettings
import importlib
importlib.reload(spcsettings)
importlib.reload(spcvars)
from pathlib import Path

# THIS FUNCTION GETS A PATH AS INPUT, READS THE PIPELINE STRUCTURE SETTINGS AND "REVERSE-ENGINEERS" IT TO EXTRACT THE SEPARATE PARTS
def structurize_path(path):
    global_project_root = Path(spcvars.global_project_root).as_posix()
    proj_structure_json = spcvars.project_structure_json

    # CUT THE INPUT PATH INTO ITS PIECES
    path = Path(path).as_posix()
    path_pieces = path.split('/')

    # GET PROJECT NAME BASED ON PROJECT ROOT
    if global_project_root in path:
        try:
            project_name = path.split(global_project_root)[-1].split('/')[1]
            project_root = os.path.join(global_project_root, project_name)
            project_root = Path(project_root).as_posix()
            # GET ALL THE PROJECT SETTINGS
            structure_dict = spcsettings.get_all_from_project(project_root)
        except IndexError:
            print(('COULD NOT EXTRACT PIPELINE INFORMATION OUT OF FILE:\n{}'.format(path)))
            return {'complete': False}

        # SHOT / ASSET / RND ? THIS WILL BE USED AS VARIABLE NAME LATER ON
        try:
            type = path.split(project_root)[-1].split('/')[1]
            type_name = ''
            if type.count('shots'):
                type_name = 'SHOT'
            elif type.count('assets'):
                type_name = 'ASSET'
            elif type.count('rnd'):
                type_name = 'RND'
            structure_dict.update({'type': type_name})
        except IndexError:
            structure_dict.update({'complete': False})
            return structure_dict

        # GET NAME
        try:
            splitter = os.path.join(project_root, type)
            splitter = Path(splitter).as_posix()
            name = path.split(splitter)[-1].split('/')[1]
            structure_dict.update({'name': name})
        except IndexError:
            structure_dict.update({'complete': False})
            return structure_dict

        # GET TASK
        try:
            splitter = os.path.join(splitter, name)
            splitter = Path(splitter).as_posix()
            task = path.split(splitter)[-1].split('/')[1]
            structure_dict.update({'task': task})
        except IndexError:
            structure_dict.update({'complete': False})
            return structure_dict

        # GET FILE
        try:
            splitter_fragments = [splitter, task, 'houdini']
            splitter = os.path.join(*splitter_fragments)
            splitter = Path(splitter).as_posix()
            file_name_full = path.split(splitter)[-1].split('/')[1]
            file_name = os.path.splitext(file_name_full)[0]
            file_extension = os.path.splitext(file_name_full)[1]
            structure_dict.update({'file_name': file_name, 'file_extension': file_extension})
        except IndexError:
            structure_dict.update({'complete': False})
            return structure_dict

        # COMPLETED
        structure_dict.update({'complete': True})
        return structure_dict
    # NOT IN PROJECT ROOT AT ALL
    else:
        print(('FILE IS NOT IN GLOBAL PROJECT ROOT:\nFile: {}\nRoot: {}'.format(path, global_project_root)))
        return {'complete': False}


# SLICE THE FILE NAME INTO IT'S PIPELINE PIECES AND RETURN A DICTIONARY
def structurize_file_name(file_path):
    file_name_structure_dict = {}
    main_structure_dict = structurize_path(file_path)
    project_prefix = main_structure_dict.get('project_prefix')
    type_name = main_structure_dict.get('name')
    task = main_structure_dict.get('task')

    splitter = '_'.join([project_prefix, type_name, task]) + '_'
    splitter = Path(splitter).as_posix()
    full_file_name = os.path.basename(file_path)
    # NAME SHORT WITH VERSION BUT NO FILE EXTENSION)
    name_short = os.path.splitext(full_file_name.split(splitter)[-1])[0]
    file_name_structure_dict.update({'name_short': name_short})

    # NAME SHORT VERSIONLESS (eg USED AS DISPLAY NAME IN THE CTRL HUB FILE LIST)
    name_short_versionless = name_short.rsplit('_', 1)[0]
    file_name_structure_dict.update({'name_short_versionless': name_short_versionless})

    # NAME LONG WITHOUT FILE EXTENSION
    name_long = os.path.splitext(full_file_name)[0]
    file_name_structure_dict.update({'name_long': name_long})

    # NAME LONG VERSIONLESS
    name_long_versionless = os.path.splitext(full_file_name)[0].rsplit('_', 1)[0]
    file_name_structure_dict.update({'name_long_versionless': name_long_versionless})

    # VERSION
    version = os.path.splitext(full_file_name)[0].rsplit('_', 1)[-1]
    file_name_structure_dict.update({'version': version})

    # NUM VERSION
    num_version = version.lstrip('v')
    file_name_structure_dict.update({'num_version': num_version})

    # FILE EXTENSION
    file_ext = os.path.splitext(full_file_name)[-1]
    file_name_structure_dict.update({'file_ext': file_ext})

    return file_name_structure_dict

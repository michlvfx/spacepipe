import hou
import os
import spc_global_pipeline_vars as spcvars
import spc_get_dir
import spc_render_asset_utils
import importlib
importlib.reload(spcvars)
importlib.reload(spc_get_dir)
importlib.reload(spc_render_asset_utils)


# IMPORT THE PROJECT SPECIFIC RENDER ASSET TEMPLATE
def get_render_asset_template(set_focus=False):
    project_root = hou.getenv('PROJECT_ROOT')
    proj_render_asset_template_dir = spc_get_dir.get_project_dir_by_keyword(project_root, 'render_asset_template')
    render_asset_template_file = os.path.join(proj_render_asset_template_dir, 'empty_render_asset.hiplc')

    # CHECK IF THERE IS ALREADY A RENDER ASSET SUBNET
    existing_render_asset = spc_render_asset_utils.get_render_asset_subnet()
    if not existing_render_asset:
        # CHECK IF TEMPLATE FILE EXISTS
        if os.path.isfile(render_asset_template_file):
            # CHECK FOR RENDER ASSET NETWORK BOX
            target_network = hou.node('/obj/')
            all_netboxes = target_network.networkBoxes()
            target_netbox = None
            for netbox in all_netboxes:
                if netbox.comment().count('RENDER ASSET'):
                    target_netbox = netbox

            # IF TARGET NETWORK BOX WAS FOUND, PLACE THE RENDER ASSET SUBNET INTO THIS NETBOX
            if target_netbox:
                node_target_pos = [target_netbox.position()[0] + 2.2, target_netbox.position()[1] + 0.8]
            # OTHERWISE USE DEFAULT POS (THAT WOULD FIT TO MY INITIALLY PLACED NETWORK BOX)
            else:
                node_target_pos = [10.6, -10.87]

            # IMPORT THE RENDER ASSET TEMPLATE FILE INTO THIS HIPFILE
            hou.hipFile.merge(render_asset_template_file)

            # GET THE JUST IMPORTED SUBNETWORK AND PLACE IT TO ITS TARGET POSITION
            render_asset_subnet_node = spc_render_asset_utils.get_render_asset_subnet()
            render_asset_subnet_node.setPosition(node_target_pos)
            if target_netbox:
                target_netbox.addNode(render_asset_subnet_node)

            # FOCUS?
            if set_focus:
                render_asset_subnet_node.setSelected(True)
                p = hou.ui.paneTabOfType(hou.paneTabType.NetworkEditor)
                p.setCurrentNode(render_asset_subnet_node)
                p.homeToSelection()

        # COULD NOT FIND TEMPLATE FILE
        else:
            hou.ui.displayMessage('Could not find Render Asset Template File!', title='Error',
                                  severity=hou.severityType.Error, details='Missing File: {}'.format(render_asset_template_file), details_expanded=True)

    # RENDER ASSET TEMPLATE FILE ALREADY EXISTS
    else:
        hou.ui.displayMessage('Render Asset Template already exists in this file!', title='Error',
                              severity=hou.severityType.Error, details='Node: {}'.format(existing_render_asset), details_expanded=True)

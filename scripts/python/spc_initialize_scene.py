import os
import hou
import nodesearch
import spc_structurize
import spc_read_settings as spcsettings
import spc_get_dir
import importlib
importlib.reload(spc_structurize)
importlib.reload(spcsettings)
importlib.reload(spc_get_dir)
from pathlib import Path


# SET HOUDINI ENVIRONMENT VARIABLES
def set_variables(file_path):
    structure_dict = spc_structurize.structurize_path(file_path)

    if structure_dict.get('complete'):

        pipe_globals = ['PROJECT_ROOT', 'PROJECT_NAME', 'PROJECT_PREFIX', 'FPS', 'RES_WIDTH', 'RES_HEIGHT',
                          'SHOT', 'ASSET', 'RND', 'TASK', 'RANGE_FROM', 'RANGE_TO', 'TEXTURES_DIR', 'SOURCE_DIR', 'OTLS_DIR', 'FLIPBOOKS_DIR',
                        'CACHES_DIR']
        # UNSET THE VARIABLES FIRST
        for var in pipe_globals:
            hou.unsetenv(var)

        # JOB
        job_dir = os.path.dirname(file_path)
        hou.putenv('JOB', job_dir)

        # PROJECT DATA
        project_root = os.path.normpath(structure_dict.get('project_root'))
        hou.putenv('PROJECT_ROOT', project_root)
        project_name = structure_dict.get('project_name')
        hou.putenv('PROJECT_NAME', project_name)
        project_prefix = structure_dict.get('project_prefix')
        hou.putenv('PROJECT_PREFIX', project_prefix)
        project_fps = structure_dict.get('project_fps')
        hou.putenv('FPS', project_fps)
        project_res_w = structure_dict.get('project_res_width')
        hou.putenv('RES_WIDTH', project_res_w)
        project_res_h = structure_dict.get('project_res_height')
        hou.putenv('RES_HEIGHT', project_res_h)

        # SHOT / ASSET / RND DATA
        type = structure_dict.get('type')
        name = structure_dict.get('name')
        hou.putenv(type, name)

        # TASK
        task = structure_dict.get('task')
        hou.putenv('TASK', task)

        # READ AND SET THE SHOT / ASSET / RND SPECIFIC SETTINGS
        file_path_n = os.path.normpath(file_path)
        splitter_n_fragments = [task, 'houdini', os.path.basename(file_path)]
        splitter_n = os.path.normpath(os.path.join(*splitter_n_fragments))
        type_dir = file_path_n.split(splitter_n)[0]
        type_settings_dict = spcsettings.get_all_from_type(type_dir)

        if type_settings_dict:
            range_from = type_settings_dict.get('range_from')
            hou.putenv('RANGE_FROM', range_from)
            range_to = type_settings_dict.get('range_to')
            hou.putenv('RANGE_TO', range_to)
            res_width = type_settings_dict.get('res_width')
            hou.putenv('RES_WIDTH', res_width)
            res_height = type_settings_dict.get('res_height')
            hou.putenv('RES_HEIGHT', res_height)

            # ADDITIONAL HOUDINI TASK DIRECTORIES
            otls_dir = os.path.join(job_dir, '_otls')
            otls_dir = Path(otls_dir).as_posix()
            hou.putenv('OTLS_DIR', otls_dir)
            caches_dir = os.path.join(job_dir, '_caches')
            caches_dir = Path(caches_dir).as_posix()
            hou.putenv('CACHES_DIR', caches_dir)
            flipbooks_dir = os.path.join(job_dir, '_flipbooks')
            flipbooks_dir = Path(flipbooks_dir).as_posix()
            hou.putenv('FLIPBOOKS_DIR', flipbooks_dir)

            # ADDITIONAL TYPE DIRECTORIES
            textures_dir = spc_get_dir.get_type_dir_by_keyword(type_dir, '_textures')
            hou.putenv('TEXTURES_DIR', textures_dir)
            source_dir = spc_get_dir.get_type_dir_by_keyword(type_dir, '_source')
            hou.putenv('SOURCE_DIR', source_dir)


            # EDIT THE SPACEPIPE_GLOBALS NULL NODE IN THE SCENE
            spacepipe_null = get_spacepipe_globals_node()
            validate_spacepipe_globals_node(spacepipe_null, pipe_globals)


# FIND THE SPACEPIPE_GLOBALS NULL
def get_spacepipe_globals_node():
    spacepipe_globals_node = ''
    current_network = hou.node('/obj/')
    type_matcher = nodesearch.NodeType('null')
    name_matcher = nodesearch.Name('SPACEPIPE_GLOBALS', exact=False)
    all_matchers = nodesearch.Group([type_matcher, name_matcher])
    spacepipe_globals_nodes = all_matchers.nodes(current_network)

    if (spacepipe_globals_nodes):
        spacepipe_globals_node = spacepipe_globals_nodes[0]
    else:
        hou.ui.displayMessage('Could not find SPACEPIPE_GLOBALS Null in this Scene!', title='Warning',
                              severity=hou.severityType.Warning)
    return(spacepipe_globals_node)


# VALIDATE ENTRIES IN THE SPACEPIPE_GLOBALS NULL
def validate_spacepipe_globals_node(spacepipe_null, pipe_globals):
    # GET ALL EXISTING PARMS
    parm_template_group = spacepipe_null.parmTemplateGroup()
    existing_parms = parm_template_group.entries()
    existing_globals_parm_names = [i.name() for i in existing_parms if str(i.type()).count('parmTemplateType.String')]
    existing_globals_parms = [i for i in spacepipe_null.parms() if i.name() in existing_globals_parm_names]
    existing_globals = [i.unexpandedString().lstrip('$') for i in existing_globals_parms]
    missing_globals = list(set(pipe_globals) - set(existing_globals))

    # CREATE MISSING GLOBALS IF THERE ARE ANY
    if (missing_globals):
        for i in missing_globals:
            parm_name = i.lower()
            parm_label = i
            parm_default_val = '${}'.format(i)
            parm_template = hou.StringParmTemplate(parm_name, parm_label, 1, default_value=(parm_default_val,))

            # APPEND TO EXISTING PARM TEMPLATE GROUP
            parm_template_group.append(parm_template)

            print(('SPACEPIPE: Added {} to SPACEPIPE_GLOBALS NULL'.format(i)))

        # APPLY EDITED PARM TEMPLATE GROUP TO GLOBAL SPACEPIPE NULL
        spacepipe_null.setParmTemplateGroup(parm_template_group)
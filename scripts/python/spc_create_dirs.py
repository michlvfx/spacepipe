import os
import json
import spc_extract_dir_structure
import spc_global_pipeline_vars as spvars
import spc_get_dir
import importlib
importlib.reload(spc_extract_dir_structure)
importlib.reload(spvars)
importlib.reload(spc_get_dir)
from pathlib import Path

# CREATE SHOT DIRS
def create_shot(project_root, shot_name, settings_dict=None):
    return_dict = {}
    new_shot_dir = spc_get_dir.get_shot_dir(project_root, shot_name)
    importlib.reload(spc_extract_dir_structure)  # THIS RELOAD IS IMPORTANT TO NOT HAVE THE GLOBAL VARIABLE INSIDE THAT SCRIPT BE DIRTY
    # FETCH SHOT DIRECTORIES TO CREATE FROM type_dir_structure.json
    type_structure_json = spvars.type_structure_json
    dirs_to_create = spc_extract_dir_structure.unpack_json_data(structure_json_file=type_structure_json, root_dir=new_shot_dir)
    # CREATE DIRECTORIES
    try:
        for i in dirs_to_create:
            os.makedirs(i)
            return_dict = {'success': 'Shot "{}" created!'.format(shot_name)}
        # CREATE TYPE SPECIFIC SETTINGS JSON FILE
        if settings_dict:
            settings_file = os.path.join(new_shot_dir, '_settings/settings.json')
            settings_file = Path(settings_file).as_posix()
            write_to_json_file(settings_file, settings_dict)
    except:
       return_dict = {'error': 'Could not create Shot folder(s)!'}

    return return_dict


# CREATE ASSET DIRS
def create_asset(project_root, asset_name, settings_dict=None):
    return_dict = {}
    new_asset_dir = spc_get_dir.get_asset_dir(project_root, asset_name)
    importlib.reload(spc_extract_dir_structure)  # THIS RELOAD IS IMPORTANT TO NOT HAVE THE GLOBAL VARIABLE INSIDE THAT SCRIPT BE DIRTY
    # FETCH ASSET DIRECTORIES TO CREATE FROM type_dir_structure.json
    type_structure_json = spvars.type_structure_json
    dirs_to_create = spc_extract_dir_structure.unpack_json_data(structure_json_file=type_structure_json, root_dir=new_asset_dir)
    # CREATE DIRECTORIES
    try:
        for i in dirs_to_create:
            os.makedirs(i)
            return_dict = {'success': 'Asset "{}" created!'.format(asset_name)}
        # CREATE TYPE SPECIFIC SETTINGS JSON FILE
        if settings_dict:
            settings_file = os.path.join(new_asset_dir, '_settings/settings.json')
            settings_file = Path(settings_file).as_posix()
            write_to_json_file(settings_file, settings_dict)
    except:
        return_dict = {'error': 'Could not create Asset folder(s)!'}

    return return_dict


# CREATE RND DIRS
def create_rnd(project_root, rnd_name, settings_dict=None):
    return_dict = {}
    new_rnd_dir = spc_get_dir.get_rnd_dir(project_root, rnd_name)
    importlib.reload(spc_extract_dir_structure)  # THIS RELOAD IS IMPORTANT TO NOT HAVE THE GLOBAL VARIABLE INSIDE THAT SCRIPT BE DIRTY
    # FETCH RND DIRECTORIES TO CREATE FROM type_dir_structure.json
    type_structure_json = spvars.type_structure_json
    dirs_to_create = spc_extract_dir_structure.unpack_json_data(structure_json_file=type_structure_json, root_dir=new_rnd_dir)
    # CREATE DIRECTORIES
    try:
        for i in dirs_to_create:
            os.makedirs(i)
            return_dict = {'success': 'Rnd "{}" created!'.format(rnd_name)}
        # CREATE TYPE SPECIFIC SETTINGS JSON FILE
        if settings_dict:
            settings_file = os.path.join(new_rnd_dir, '_settings/settings.json')
            settings_file = Path(settings_file).as_posix()
            write_to_json_file(settings_file, settings_dict)
    except:
        return_dict = {'error': 'Could not create Rnd folder(s)!'}

    return return_dict


# CREATE TASK DIRS
def create_task(root_dir, task_name):
    importlib.reload(spc_extract_dir_structure)
    return_dict = {}
    new_task_dir = os.path.join(root_dir, task_name)
    new_task_dir = Path(new_task_dir).as_posix()
    # FETCH TASK DIRECTORIES TO CREATE FROM task_dir_structure.json
    task_structure_json = spvars.task_structure_json
    dirs_to_create = spc_extract_dir_structure.unpack_json_data(structure_json_file=task_structure_json, root_dir=new_task_dir)
    # CREATE DIRECTORIES
    try:
        for i in dirs_to_create:
            os.makedirs(i)
            return_dict = {'success': 'Task "{}" created!'.format(task_name)}
    except:
        return_dict = {'error': 'Could not create Task folder(s)!'}

    return return_dict




# WRITE TO JSON FILE
def write_to_json_file(settings_file, settings_dict):
    with open(settings_file, 'w') as f:
        json.dump(settings_dict, f, indent=4, sort_keys=True)

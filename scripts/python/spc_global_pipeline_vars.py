import hou
import os
import sys

try:
    pipeline_root = hou.getenv('SPACEPIPE_ROOT')
    global_project_root = hou.getenv('PROJECTROOT')
    project_structure_json = os.path.join(os.path.join(pipeline_root, '_settings'), 'project_dir_structure.json')
    type_structure_json = os.path.join(os.path.join(pipeline_root, '_settings'), 'type_dir_structure.json')
    task_structure_json = os.path.join(os.path.join(pipeline_root, '_settings'), 'task_dir_structure.json')
except Exception as error_message:
    error_str = repr(error_message)
    try:
        hou.ui.displayMessage('Could not retrieve global Pipeline Variables. Please ask Pipeline Dude!', title='ERROR',
                              severity=hou.severityType.Error, details=error_str)
    except:
        sys.stdout.write('COULD NOT RETRIEVE GLOBAL PIPELINE VARIABLES!')

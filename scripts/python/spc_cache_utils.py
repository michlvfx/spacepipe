import spc_global_pipeline_vars as spcvars
import spc_get_dir
import hou
import os
import platform
import subprocess
import nodesearch
import pdg
import importlib
importlib.reload(spcvars)
importlib.reload(spc_get_dir)

# COLORS
main_color_static = (0.565, 0.494, 0.863)
main_color_active = (0.576, 0.208, 0.475)
active_color_rgb_1 = (0.765, 1, 0.576)
active_color_rgb_2 = (1, 0.529, 0.624)
active_color_rgb_3 = (0.145, 0.667, 0.557)
active_color_rgb_4 = (0.71, 0.518, 0.004)
inactive_color_rgb = (0.3, 0.3, 0.3)


# SET HDA NODE COLOR
def set_hda_node_color(hda_node, mode='inactive'):
    col = hou.Color(inactive_color_rgb)
    if mode == 'static':
        col = hou.Color(main_color_static)
    elif mode == 'active':
        col = hou.Color(main_color_active)
    hda_node.setColor(col)


# GET THE MAIN TOPNET NODE OBJECT
def get_main_topnet_node(hda_node):
    topnet_node = hou.node(hda_node.path() + '/topnet1')
    return topnet_node


# GET THE ROPGEOMETRY NODE INSIDE THE MAIN TOPNET
def get_top_ropgeometry_node(hda_node):
    topnet_node = get_main_topnet_node(hda_node)
    ropgeometry_node = hou.node(topnet_node.path() + '/ropgeometry1')
    return ropgeometry_node


# GET THE MAIN WEDGE NODE INSIDE THE MAIN TOPNET
def get_main_wedge_top_node(hda_node):
    topnet_node = get_main_topnet_node(hda_node)
    wedge_node = hou.node(topnet_node.path() + '/wedge_main')
    return wedge_node


# GET ALL FILE NODES INSIDE HDA
def get_file_nodes(hda_node):
    network = hda_node
    type_matcher = nodesearch.NodeType('file')
    all_file_nodes = type_matcher.nodes(network)
    return(all_file_nodes)


# GET ALL FILE MERGE NODES INSIDE HDA
def get_file_merge_nodes(hda_node):
    network = hda_node
    type_matcher = nodesearch.NodeType('filemerge')
    all_file_merge_nodes = type_matcher.nodes(network)
    return(all_file_merge_nodes)


# GET ACTIVE INPUTS
def get_active_inputs(hda_node):
    hda_inputs = hda_node.inputs()
    active_inputs = []
    for index, i in enumerate(hda_inputs):
        if i:
            active_inputs.append(index)
    return active_inputs


# PDG GRAPH COMPLETION CALLBACK
def pdg_complete_event(handler, event):
    # GET THE HDA NODE FROM THE TRIGGERED EVENT
    print('COMPLETED PDG GRAPH EXECUTION!\n')
    # Deregister the handler from all events its listening to
    handler.removeFromAllEmitters()


# WRITE CACHE LOCALLY
def write_cache_button_clicked(kwargs):
    hda_node = kwargs.get('node')
    topnet_node = get_main_topnet_node(hda_node)
    wedge_node = get_main_wedge_top_node(hda_node)
    filecache_node = get_main_topnet_node(hda_node)
    update_cache_path(kwargs)

    # SANITY CHECKS
    valid = sanity_checks(kwargs)
    if valid:
        # CHECK HOW MANY INPUTS ARE WIRED INTO THE HDA AND ADJUST THE NUMBER OF WEDGES ACCORDINGLY
        wedge_num_list = get_active_inputs(hda_node)
        num_wedges = len(wedge_num_list)

        # SET WEDGE ATTRIBUTES
        wedge_node.parm('wedgecount').set(num_wedges)
        wedge_node.parm('values1').set(num_wedges)
        for index, num in enumerate(wedge_num_list):
            wedge_node.parm('intvalue1_{}'.format(index+1)).set(num)

        # DIRTY TOP GRAPH
        topnet_node.dirtyAllTasks(0)
        hou.hipFile.save()

        # PRE COOK THE GRAPH TO ACCESS THE UNDERNEATH DATA FOR THE EVENT
        filecache_node.executeGraph(False, False, False, True)
        pdg_context = get_top_ropgeometry_node(hda_node).getPDGGraphContext()
        context_handler = pdg_context.addEventHandler(pdg_complete_event, pdg.EventType.CookComplete, pass_handler=True)

        # EXECUTE TOP GRAPH
        filecache_node.executeGraph(0, 0)


# GET CACHE TYPE
def get_cache_type(hda_node):
    cache_type = hda_node.parm('type').eval()
    extension = ''
    type_name = ''
    if cache_type == 0:
        extension = '.bgeo.sc'
        type_name = 'bgeo'
    elif cache_type == 1:
        extension = '.abc'
        type_name = 'alembic'
    elif cache_type == 2:
        extension = '.vdb'
        type_name = 'vdb'

    cache_type_dict = {type_name: extension}
    return cache_type_dict


# GENERATE CACHE PATH
def create_cache_path(kwargs, single_frame=False):
    hda_node = kwargs.get('node')
    hda_node_name = hda_node.name()
    cache_name = hda_node.parm('name').eval()

    # GET CACHE TYPE
    cache_type_info = get_cache_type(hda_node)
    type_name = list(cache_type_info.keys())[0]
    extension = list(cache_type_info.values())[0]

    # CREATE NEW PATH STRING
    file_basename = os.path.splitext(os.path.basename(hou.hipFile.path()))[0]
    cache_folder_name = hda_node.path().replace('/obj/', '').rsplit('/', 1)[0] + '-' + cache_name
    job_dir = hou.getenv('JOB')

    cache_path_base = job_dir + '/_caches/' + file_basename + '/' + cache_folder_name + '/' + type_name + '/' + cache_name + '_stream_`@stream_num`'

    # SINGLE FRAME OR SEQUENCE?
    if not single_frame and type_name is not 'alembic':
        cache_path = cache_path_base + '.$F4' + extension
    else:
        cache_path = cache_path_base + extension

    return cache_path


# SET CACHE PATH
def set_cache_path(kwargs, cache_path):
    hda_node = kwargs.get('node')
    file_parm = hda_node.parm('file')
    file_parm.lock(False)

    # SET NEW CACHE PATH TO HDA
    file_parm.set(cache_path)
    file_parm.lock(True)

    # SET TOP ROPGEOMETRY OUTPUT PATH
    set_top_ropgeometry_cache_path(hda_node, cache_path)

    # UPDATE THE FILE NODE PATHS THAT READ BACK THE CACHED GEO
    set_file_node_cache_paths(hda_node, cache_path)


# SET TOP ROPGEOMETRY CACHE OUTPUT PATH
def set_top_ropgeometry_cache_path(hda_node, cache_path):
    # PREPARE CACHE PATH SPLITS
    cache_stream_split = cache_path.split('`@stream_num`')
    cache_stream_front = cache_stream_split[0] + '`@stream_num`'
    cache_stream_back = cache_stream_split[-1]

    # SET TO ROPGEOMETRY TOP OUT FILE PATH
    # CHECK IF USER WEDGES ARE ENABLED
    if hda_node.parm('user_wedge').eval():
        cache_path = cache_stream_front + '_wedge_`@wedgenum`' + cache_stream_back

    ropgeo_node = get_top_ropgeometry_node(hda_node)
    ropgeo_node.parm('sopoutput').set(cache_path)


# SET THE FILE AND FILE MERGE NODE PATHS
def set_file_node_cache_paths(hda_node, cache_path):
    # PREPARE CACHE PATH SPLITS
    cache_stream_split = cache_path.split('`@stream_num`')
    cache_stream_front = cache_stream_split[0] + '`@stream_num`'
    cache_stream_back = cache_stream_split[-1]

    file_nodes = get_file_nodes(hda_node)
    file_merge_nodes = get_file_merge_nodes(hda_node)

    # FILE NODES
    for file_node in file_nodes:
        node_name = file_node.name()
        stream_num = node_name.split('READ_STREAM_', 1)[-1].split('_', 1)[0]
        cache_path_stream = cache_stream_front.replace('`@stream_num`', stream_num) + cache_stream_back
        file_node.parm('file').set(cache_path_stream)

    # FILE MERGE NODES (USER WEDGES)
    for merge_node in file_merge_nodes:
        node_name = merge_node.name()
        stream_num = node_name.split('READ_STREAM_', 1)[-1].split('_', 1)[0]
        cache_path_wedged = cache_stream_front.replace('`@stream_num`', stream_num) + '_wedge_$SLICE' + cache_stream_back
        merge_node.parm('file').set(cache_path_wedged)


# SET CACHE PATH TO SINGLE FILE
def set_cache_path_single_frame(kwargs):
    cache_path = create_cache_path(kwargs, single_frame=True)
    set_cache_path(kwargs, cache_path)


# SET CACHE PATH TO SEQUENCE
def set_cache_path_sequence(kwargs):
    cache_path = create_cache_path(kwargs, single_frame=False)
    set_cache_path(kwargs, cache_path)


# UPDATE CACHE PATH
def update_cache_path(kwargs, set_path=True):
    single_frame = False
    hda_node = kwargs.get('node')
    # READ ONLY?
    read_only = hda_node.parm('read_only').eval()
    if read_only:
        pass
    else:
        # SEQUENCE OR SINGLE FRAME?
        framegeneration = hda_node.parm('framegeneration').eval()
        if framegeneration < 1:
            single_frame = True

    cache_path = create_cache_path(kwargs, single_frame=single_frame)

    # SET THE PATH OR ONLY RETURN THE UPDATED VALUE?
    if set_path:
        set_cache_path(kwargs, cache_path)

    return cache_path

# CREATE ABSOLUTE READ BUTTON CLICKED
def create_abs_read_button_clicked(hda_node):
    # GET THE "READ SETUP" NETWORKBOX INSIDE THE SPACECACHER
    netbox_name = 'READ_SETUP'
    network = hda_node
    # CONFORM THE INPUT OUTPUT COLORS OF THE HDA
    update_input_output_colors(hda_node)
    # UNLOCK
    hda_node.allowEditingOfContents()
    cache_name = hda_node.evalParm('name')
    parent_network = network.parent()
    # THE NAME OF THE NETWORK BOX WAS SET VIA PYTHON (I DONT KNOW IF ITS POSSIBLE TO SET IT IN THE INTERFACE)
    read_setup_netbox = network.findNetworkBox(netbox_name)
    # CLONE THE READ SETUP NETWORKBOX WITH ALL ITS CONTENT
    copied_read_netbox = network.copyNetworkBox(read_setup_netbox)
    # ITERATE OVER ALL THE ITEMS IN THE CLONED NETWORKBOX UND SET THEIR PATHS TO BE ABSOLUTE
    # ALSO KILL ALL EXPRESSIONS THAT WOULD STILL LINK TO THE SPACE CACHER
    netbox_nodes = copied_read_netbox.nodes()
    target_nodetypes = ['file', 'filemerge']

    for node in netbox_nodes:
        # MAKE PATHS ABSOLUTE
        if node.type().name() in target_nodetypes:
            file_parm = node.parm('file')
            unexpanded_filepath = file_parm.unexpandedString()
            expanded_job_dir = hou.getenv('JOB')
            expanded_filepath = unexpanded_filepath.replace('$JOB', expanded_job_dir)
            # SET EXPANDED FILEPATH
            file_parm.set(expanded_filepath)

        # KILL ALL EXPRESSIONS
        for parm in node.parms():
            parm.deleteAllKeyframes()

    # CREATE NEW SUBNETWORK AND MOVE THE CLONED NETWORKBOX OVER THERE
    read_subnet = parent_network.createNode('subnet', node_name='READ_CACHES_{}'.format(cache_name))
    hou.moveNodesTo(copied_read_netbox.nodes(), read_subnet)
    hda_color = hda_node.color()
    read_subnet.setColor(hda_color)

    # KILL LEFTOVER NETWORKBOX IN SPACECACHER HDA
    copied_read_netbox.destroy()

    # LOCK HDA
    hda_node.matchCurrentDefinition()

    # SELECT AND SET FOCUS TO NEWLY CREATED SUBNET
    read_subnet.setSelected(True)
    p = hou.ui.paneTabOfType(hou.paneTabType.NetworkEditor)
    p.setCurrentNode(read_subnet)
    p.homeToSelection()

    # CONFORM THE INPUT OUTPUT COLORS OF THE HDA AGAIN
    update_input_output_colors(hda_node)


# RELOAD GEOMETRY BUTTON CLICKED
def reload_geometry_button_clicked(hda_node):
    # GET ALL FILE NODES INSIDE THE HDA AND TRIGGER THEIR RELOAD BUTTONS
    network = hda_node

    # FILE NODES
    type_file_matcher = nodesearch.NodeType('file')
    all_file_nodes = type_file_matcher.nodes(network)
    for i in all_file_nodes:
        reload_button_parm = i.parm('reload')
        reload_button_parm.pressButton()

    # FILE MERGE NODES / USER WEDGES
    type_filemerge_matcher = nodesearch.NodeType('filemerge')
    all_filemerge_nodes = type_filemerge_matcher.nodes(network)
    for i in all_filemerge_nodes:
        reload_button_parm = i.parm('reload')
        reload_button_parm.pressButton()

    update_input_output_colors(hda_node)


# TYPE MENU CHANGED
def type_changed(kwargs):
    update_cache_path(kwargs)


# USER WEDGE TOGGLED
def user_wedge_toggled(kwargs):
    hda_node = kwargs.get('node')
    update_cache_path(kwargs)
    update_input_output_colors(hda_node)

    # UPDATE THE CHILD NODES
    child_nodes = [i.node() for i in hda_node.parmsReferencingThis() if not i.node() is hda_node and hda_node.path() not in i.path()]
    child_nodes = list(set(child_nodes))
    for child_node in child_nodes:
        if child_node.type().name().count('space_cacher'):
            # UPDATE COLORS
            update_input_output_colors(child_node)


# NAME CHANGED
def name_changed(kwargs):
    hda_node = kwargs.get('node')
    old_name = hda_node.parm('source_name').eval()
    new_name = hda_node.parm('name').eval()

    # CHECK IF THIS IS A CHILD NODE
    if hda_node.parm('ischild').eval() == 0:
        # CHECK IF THERE IS ALREADY A SPACE CACHER NODE WITH THE SAME NAME PARM
        current_network = hou.node(hda_node.path().rsplit('/', 1)[0])
        type_matcher = nodesearch.NodeType('space_cacher')
        name_parm_matcher = nodesearch.Parm('name', '==', new_name)
        all_matchers = nodesearch.Group([type_matcher, name_parm_matcher])
        same_space_cachers = all_matchers.nodes(current_network)

        double = False
        # REMOVE CURRENT NODE FROM THE LIST
        try:
            same_space_cachers.remove(hda_node)
        except:
            pass

        # GET ALL CHILD NODES THAT ARE REFERENCING THAT NAME AND EXCLUDE THEM FROM THE "same_space_cachers" LIST
        name_ref_nodes = [i.node() for i in hda_node.parm('name').parmsReferencingThis()]
        same_space_cachers = [i for i in same_space_cachers if not i in name_ref_nodes]

        # STILL? THEN ITS INVALID
        if same_space_cachers:
            double = True

        # EXECUTE NAME CHANGE OPERATIONS
        if not double:
            # UPDATE THE SOURCE NAME HELPER PARM
            hda_node.setParms({'source_name': new_name})

            # SET READ CACHE OFF
            hda_node.parm('read_only').lock(False)
            hda_node.setParms({'read_only': 0})

            # UPDATE NODE COMMENT
            hda_node.setComment('WAIT TO CACHE: {}'.format(new_name))

            # UPDATE COLOR
            set_hda_node_color(hda_node, mode='static')

            # SET CACHE MODE TO NO OPERATION
            hda_node.setParms({'filemode': 3})

            # UPDATE CACHE PATH
            update_cache_path(kwargs)

            # UPDATE COMMENT
            read_cache_toggled(hda_node)

            # UPDATE THE CHILD NODES
            child_nodes = [i.node() for i in hda_node.parmsReferencingThis() if not i.node() is hda_node and hda_node.path() not in i.path()]
            child_nodes = list(set(child_nodes))
            for child_node in child_nodes:
                if child_node.type().name().count('space_cacher'):
                    # UPDATE CHILD NODES COMMENT
                    child_node.setComment('READ CACHE: {}'.format(new_name))
                    update_input_output_colors(child_node)

        # STILL DOUBLE
        else:
            # SET BACK OLD NAME
            the_other_one = ''
            for space_cacher in same_space_cachers:
                if space_cacher != hda_node:
                    the_other_node = space_cacher
                    # GET THE MASTER / SOURCE NODE AS A RESULT TO BE SHOWN TO THE USER
                    if the_other_node.parm('ischild').eval():
                        space_cacher_source_node = hou.node(the_other_node.parm('source_cacher_node').eval())
                        the_other_one = space_cacher_source_node.name()
                    else:
                        the_other_one = space_cacher.name()
            hda_node.setParms({'name': old_name})
            hou.ui.displayMessage('Cache with the same Name already exists in this Network!\n{}'.format(the_other_one),
                                  title='ERROR', severity=hou.severityType.Error)

    # ONLY UPDATE THE COMMENT IF ITS A CHILD NODE
    else:
        hda_node.setGenericFlag(hou.nodeFlag.DisplayComment, False)
        hda_node.setComment('READ CACHE: {}'.format(new_name))
        hda_node.setGenericFlag(hou.nodeFlag.DisplayComment, True)


# SANITY CHECKS
def sanity_checks(kwargs):
    valid = True
    hda_node = kwargs.get('node')
    cache_name = hda_node.parm('name').eval()

    # CHECK CACHE NAME
    if cache_name:
        if cache_name.count(' '):
            hou.ui.displayMessage('No Whitespaces in Name allowed', title='ERROR', severity=hou.severityType.Error)
            valid = False
        if cache_name.count ('.'):
            hou.ui.displayMessage('No Dots in Name allowed', title='ERROR', severity=hou.severityType.Error)
            valid = False
    else:
        hou.ui.displayMessage('Please enter a Name', title='ERROR', severity=hou.severityType.Error)
        valid = False

    # CHECK FOR INPUT(S)
    if valid:
        inputs = hda_node.inputs()
        if len(inputs) == 0:
            hou.ui.displayMessage('No Input', title='ERROR', severity=hou.severityType.Error)
            valid = False

    # CHECK IF CACHE ALREADY EXISTS
    if valid:
        cache_path = hda_node.parm('file').eval()
        cache_path = hou.expandString(cache_path)
        cache_dir = os.path.dirname(cache_path)
        name_to_check = os.path.splitext(os.path.basename(cache_path))[0].split('.', 1)[0]
        # SCAN DIR
        exists = False
        if os.path.isdir(cache_dir):
            for i in os.listdir(cache_dir):
                if i.count(name_to_check):
                    exists = True
            if exists:
                ask_override = hou.ui.displayMessage('Cache already exists!', buttons=['Override', 'Cancel'], title='Warning', severity=hou.severityType.Warning)
                if ask_override == 1:
                    valid = False

    return valid


# READ CACHE TOGGLED
def read_cache_toggled(hda_node):
    read_only = hda_node.parm('read_only').eval()
    cache_name = hda_node.parm('name').eval()
    inputs = hda_node.inputs()

    # READ ONLY MODE
    if read_only:
        hda_node.setParms({'filemode': 1})
        set_hda_node_color(hda_node, mode='static')
        hda_node.setComment('READ CACHE: {}'.format(cache_name))

    # READY TO CACHE MODE
    else:
        hda_node.setParms({'filemode': 3})
        if len(inputs):
            hda_node.setComment('WAIT TO CACHE: {}'.format(cache_name))
            set_hda_node_color(hda_node, mode='static')
        else:
            hda_node.setComment('Inactive'.format(cache_name))
            set_hda_node_color(hda_node, mode='inactive')

    # UPDATE INPUT AND OUTPUT COLORS
    update_input_output_colors(hda_node)


# UPDATE INPUT AND OUTPUT COLORS
def update_input_output_colors(hda_node):
    is_child = hda_node.parm('ischild').eval()
    read_only = hda_node.parm('read_only').eval()
    if not is_child:
        if not read_only:

            # COLOR CONNECTED INPUTS ON MASTER NODE IN CACHE MODE
            active_inputs = get_active_inputs(hda_node)
            activate_inputs(hda_node, activate=True, active_inputs=active_inputs)

            # CHECK ACTIVE OUTPUTS
            valid_outputs = get_valid_outputs(hda_node)
            activate_outputs(hda_node, activate=True, active_outputs=valid_outputs)

        # UNCOLOR ALL INPUTS IF READ ONLY ON MASTER NODE
        else:
            activate_inputs(hda_node, activate=False)
            # CHECK ACTIVE OUTPUTS
            valid_outputs = get_valid_outputs(hda_node)
            activate_outputs(hda_node, activate=True, active_outputs=valid_outputs)

    # CHILD SPACE CACHERS (READERS)
    else:
        # DEACTIVATE ALL INPUTS
        activate_inputs(hda_node, activate=False)
        # CHECK FOR VALID OUTPUTS AND COLOR THEM
        valid_outputs = get_valid_outputs(hda_node)
        activate_outputs(hda_node, activate=True, active_outputs=valid_outputs)


# GET OUTPUT NODES WITH VALID GEOMETRY FILES
def get_valid_outputs(hda_node):
    # GET CACHE NAME
    cache_name = hda_node.parm('name').eval()

    # GET CACHE EXTENSION
    cache_type_info = get_cache_type(hda_node)
    extension = list(cache_type_info.values())[0]

    # USER WEDGES ACTIVATED?
    user_wedge = hda_node.parm('user_wedge').eval()

    valid_outputs = []
    output_type_matcher = nodesearch.NodeType('output')
    network = hda_node
    output_nodes = output_type_matcher.nodes(network)
    for out_node in output_nodes:
        out_index = out_node.parm('outputidx').eval()
        input_node_tree = out_node.inputAncestors()
        target_file_node = ''
        for input in input_node_tree:
            input_node_type = input.type().name()

            # NO WEDGES
            if not user_wedge:
                if input.name().count('READ_STREAM') and input_node_type == 'file':
                    target_file_node = input

            # USER WEDGES ACTIVATED
            else:
                if input.name().count('READ_STREAM') and input.name().count('WEDGES') and input_node_type == 'filemerge':
                    target_file_node = input

        if target_file_node:
            file_path = target_file_node.parm('file').eval()
            file_dirname = os.path.dirname(file_path)
            file_basename = os.path.basename(file_path)

            # CHECK FOR VALID FILES INDEPENDENT FROM CURRENT FRAME BECAUSE TOPS DONT EVALUATE THE TIMELINE AND THEREFORE
            # WE CANT JUST CHECK IF THE FILE BEHIND THE CURRENT PATHS EVALUATION EXISTS
            if file_basename.count('{}_stream_{}'.format(cache_name, out_index)):

                # CHECK IF THIS VALID FILE PATH POINTS TO AN EXISTING FILE
                if os.path.isdir(file_dirname):
                    for i in os.listdir(file_dirname):

                        # CHECK FOR FILE EXTENSION
                        # NOT WITH os.path.splitext BECAUSE THIS WOULD CAUSE A .bgeo.sc FILE TO ONLY RETURN .sc
                        if len(i.split(extension)) > 1:

                            # CHECK FOR STREAM NUMBER
                            if len(i.split('_stream_' + str(out_index))) > 1:

                                # CHECK FOR WEDGE IF USER WEDGE IS ACTIVATED
                                if not user_wedge and not i.count('_wedge_') or user_wedge and i.count('_wedge_'):
                                    valid_outputs.append(out_index)

    return valid_outputs


# GO TO EXPLORER
def go_to_explorer_clicked(kwargs):
    hda_node = kwargs.get('node')
    cache_path = hou.expandString(hda_node.parm('file').eval())
    current_system_platform = platform.system()
    cache_path_eval = hou.expandString(cache_path)
    target_dir = os.path.dirname(cache_path_eval)

    if os.path.isdir(target_dir):
        try:
            # WINDOWS
            if current_system_platform == 'Windows':
                os.startfile(target_dir)
            # MACOS
            elif current_system_platform == 'Darwin':
                subprocess.Popen(["open", target_dir])
            # LINUX
            else:
                subprocess.Popen(["xdg-open", target_dir])
        except:
            print(('COULD NOT OPEN CACHEPATH:\n{}'.format(target_dir)))
    # NOT YET EXISTING
    else:
        hou.ui.displayMessage('Directory does not exist yet', title='ERROR', severity=hou.severityType.Error)


# ACTIVATE INPUTS
def activate_inputs(hda_node, activate=True, active_inputs=[0, 1, 2, 3]):
    # GET THE SUBNETWORK INPUT NODES
    inputs = hda_node.indirectInputs()

    for input in inputs:
        num = input.number()
        if num == 0:
            if activate and active_inputs.count(num):
                input.setColor(hou.Color(active_color_rgb_1))
            else:
                input.setColor(hou.Color(inactive_color_rgb))
        if num == 1:
            if activate and active_inputs.count(num):
                input.setColor(hou.Color(active_color_rgb_2))
            else:
                input.setColor(hou.Color(inactive_color_rgb))
        if num == 2:
            if activate and active_inputs.count(num):
                input.setColor(hou.Color(active_color_rgb_3))
            else:
                input.setColor(hou.Color(inactive_color_rgb))
        if num == 3:
            if activate and active_inputs.count(num):
                input.setColor(hou.Color(active_color_rgb_4))
            else:
                input.setColor(hou.Color(inactive_color_rgb))


# ACTIVATE OUTPUTS
def activate_outputs(hda_node, activate=True, active_outputs=[0, 1, 2, 3]):
    # GET THE SUBNETWORK OUTPUT NODES
    inputs = hda_node.indirectInputs()
    matcher = nodesearch.NodeType('output')
    network = hda_node

    for output in matcher.nodes(network):
        output_index = output.parm('outputidx').eval()
        if output_index == 0:
            if activate and active_outputs.count(output_index):
                output.setColor(hou.Color(active_color_rgb_1))
            else:
                output.setColor(hou.Color(inactive_color_rgb))
        if output_index == 1:
            if activate and active_outputs.count(output_index):
                output.setColor(hou.Color(active_color_rgb_2))
            else:
                output.setColor(hou.Color(inactive_color_rgb))
        if output_index == 2:
            if activate and active_outputs.count(output_index):
                output.setColor(hou.Color(active_color_rgb_3))
            else:
                output.setColor(hou.Color(inactive_color_rgb))
        if output_index == 3:
            if activate and active_outputs.count(output_index):
                output.setColor(hou.Color(active_color_rgb_4))
            else:
                output.setColor(hou.Color(inactive_color_rgb))


# LOAD STREAM CHANGED
def loadstream_changed(kwargs, stream_num):
    hda_node = kwargs.get('node')
    target_nodes = get_file_read_nodes(hda_node, stream_num)
    parm_val = hda_node.parm('loadtype_stream' + str(stream_num)).evalAsString()

    # SET PARMS
    for i in target_nodes:
        target_parm = i.parm('loadtype')
        target_parm.set(parm_val)


# DISPLAY PACKED STREAM CHANGED
def displaypackedstream_changed(kwargs, stream_num):
    hda_node = kwargs.get('node')
    target_nodes = get_file_read_nodes(hda_node, stream_num)
    parm_val = hda_node.parm('packedviewedit_stream' + str(stream_num)).evalAsString()

    # SET PARMS
    for i in target_nodes:
        target_parm1 = i.parm('packedviewedit')
        target_parm2 = i.parm('viewportlod')
        try:
            target_parm1.set(parm_val)
        except:
            pass
        try:
            target_parm2.set(parm_val)
        except:
            pass


# GET FILE READ NODES INSIDE HDA OF SPECIFIC STREAM NUMBER
def get_file_read_nodes(hda_node, stream_num):
    file_nodes = get_file_nodes(hda_node)
    filemerge_nodes = get_file_merge_nodes(hda_node)
    target_file = [i for i in file_nodes if i.name().count('STREAM_' + str(stream_num))][0]
    target_filemerge = [i for i in filemerge_nodes if i.name().count('STREAM_' + str(stream_num))][0]
    target_nodes = [target_file, target_filemerge]

    return target_nodes
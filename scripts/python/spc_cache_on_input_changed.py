import spc_cache_utils
import importlib
importlib.reload(spc_cache_utils)

hda_node = kwargs.get('node')

# GATHER PARMS
read_only = hda_node.parm('read_only').eval()
cache_name = hda_node.parm('name').eval()
is_child = hda_node.parm('ischild').eval()
inputs = hda_node.inputs()

# MASTER SPACE CACHER NODE
if not is_child:
    # HAS INPUT(S)
    if len(inputs):
        spc_cache_utils.set_hda_node_color(hda_node, mode='static')
        # READ MODE
        if read_only:
            hda_node.setComment('READ CACHE: {}'.format(cache_name))
        # WRITE MODE
        else:
            hda_node.setComment('WAIT TO CACHE: {}'.format(cache_name))
    # MASTER NODE BUT NO INPUT CONNECTED
    else:
        # READ MODE
        if read_only:
            hda_node.setComment('READ CACHE: {}'.format(cache_name))
            spc_cache_utils.set_hda_node_color(hda_node, mode='static')
        # WRITE MODE
        else:
            hda_node.setComment('Inactive')
            spc_cache_utils.set_hda_node_color(hda_node, mode='inactive')
# CHILD NODE
else:
    spc_cache_utils.set_hda_node_color(hda_node, mode='static')

# UPDATE THE INPUT COLORS
spc_cache_utils.update_input_output_colors(hda_node)


import spc_structurize
import spc_utils
import os
import json
import importlib
importlib.reload(spc_structurize)
importlib.reload(spc_utils)
from pathlib import Path

# WRITE TO RECENTFILE
def write(recent_dir, file_path):
    try:
        os.makedirs(recent_dir)
    except:
        pass
    # RECENT DIR EXISTS
    if os.path.isdir(recent_dir):
        structure_dict = spc_structurize.structurize_path(file_path)
        name_structure_dict = spc_structurize.structurize_file_name(file_path)
        project_name = structure_dict.get('project_name')
        recent_file = os.path.join(recent_dir, project_name + '.json')
        recent_file = Path(recent_file).as_posix()

        # GATHER DATA TO ADD
        type = structure_dict.get('type')
        name = structure_dict.get('name')
        task = structure_dict.get('task')
        file = name_structure_dict.get('name_short_versionless')
        version = name_structure_dict.get('version')
        info_dict = {'type': type, 'name': name, 'task': task, 'file': file, 'version': version}

        # WRITE DATA TO JSON FILE
        with open(recent_file, 'w') as f:
            json.dump(info_dict, f, indent=4, sort_keys=True)

    # FAIL
    else:
        print(('SOMETHING WENT WRONG WRITING TO RECENT FILE DIR:\n{}'.format(recent_dir)))


# GET LATEST RECENTFILE
def get_latest(recent_dir):
    latest_recent_file = ''
    if os.path.isdir(recent_dir):
        recent_files = []
        for i in spc_utils.remove_unwanted_folders(os.listdir(recent_dir)):
            if os.path.splitext(i)[-1] == '.json':
                new_recent_file = os.path.join(recent_dir, i)
                new_recent_file = Path(new_recent_file).as_posix()
                recent_files.append(new_recent_file)

        # SORT BY DATE
        recent_files.sort(key=os.path.getmtime)
        latest_recent_file = recent_files[-1]

    return latest_recent_file



# READ RECENTFILE
def read(recent_file):
    recent_info_dict = {}
    # READ RECENT FILE
    if os.path.isfile(recent_file):
        with open(recent_file, 'r') as f:
            recent_info_dict = json.load(f)
    # FAIL
    else:
        print(('COULD NOT FIND RECENTFILE:\n{}'.format(recent_file)))

    return recent_info_dict

import os


def remove_unwanted_folders(all_folders):
    unwanted = ['.mayaSwatches', '._.DS_Store', 'backup', '.DS_Store', 'BACKUP', 'THUMB', 'thumb', '_OLD', 'Thumbs.db', 'Keyboard', '.vrayThumbs', '_settings', '_source', '_textures']
    for i in unwanted:
        try:
           all_folders.remove(i)
        except:
           pass
    for i in all_folders:
        if i.count('DS_Store'):
            all_folders.remove(i)
        if str(i).startswith('_'):
            all_folders.remove(i)
        elif i.count('autosave'):
            all_folders.remove(i)
        elif i.startswith('tmp'):
            try:
                all_folders.remove(i)
            except:
                pass
    return all_folders

import hou
import os
import nodesearch
import spc_structurize
import spc_get_dir
import importlib
importlib.reload(spc_structurize)
importlib.reload(spc_get_dir)
from pathlib import Path

# CREATE PSEUDO HDA INFO DICT THAT USES THE PIPELINE DATA FROM THE CURRENT HIPFILE
# TO CREATE THE HDA RELEVANT STRING FRAGMENTS
def create_hda_info_from_hip(hipfile=None):
    # USE CURRENT HIPFILE IF NO OTHER IS GIVEN
    if not hipfile:
        hipfile = hou.getenv('HIPFILE')

    # EXTRACT PIPELINE DATA FROM THE HIP FILE PATH
    # GET OTL DIR
    task_dir = os.path.dirname(hipfile).rsplit('/', 1)[0]
    otl_dir = spc_get_dir.get_task_dir_by_keyword(task_dir, 'otls')
    structure_dict = spc_structurize.structurize_file_name(hipfile)

    # CREATE HDA FRAGMENTS DICT OUT OF EXTRACTED DATA
    hda_type_name = structure_dict.get('name_short_versionless')
    hda_namespace = structure_dict.get('name_long_versionless').rsplit('_' + hda_type_name, 1)[0]
    hda_version = structure_dict.get('num_version')
    hda_name = '{}::{}::{}'.format(hda_namespace, hda_type_name, hda_version)
    hda_file_name = '{}_{}_v{}.hda'.format(hda_namespace, hda_type_name, hda_version)
    hda_full_path = os.path.join(otl_dir, hda_file_name).replace('\\', '/')
    hda_full_path = Path(hda_full_path).as_posix()
    hda_node_name = os.path.splitext(hda_file_name)[0]

    # APPEND ALL INFO TO DICT
    hda_info_dict = {}
    hda_info_dict.update({'hda_type_name': hda_type_name})
    hda_info_dict.update({'hda_namespace': hda_namespace})
    hda_info_dict.update({'hda_version': hda_version})
    hda_info_dict.update({'hda_name': hda_name})
    hda_info_dict.update({'hda_file_name': hda_file_name})
    hda_info_dict.update({'hda_full_path': hda_full_path})
    hda_info_dict.update({'hda_node_name': hda_node_name})

    return hda_info_dict


# GET UNCONVERTED RENDER ASSET SUBNETWORK
def get_render_asset_subnet(network='/obj/'):
    search_network = hou.node(network)
    type_matcher = nodesearch.NodeType('subnet')
    all_subnets = type_matcher.nodes(search_network)
    render_asset_parm_name = 'render_asset'

    render_asset_subnet = None

    # CHECK FOR "render_asset" PARM
    for subnet in all_subnets:
        if subnet.parmTuple(render_asset_parm_name):
            render_asset_subnet = subnet
            break

    return render_asset_subnet


# GET RENDER ASSET CORRESPONDING TO CURRENT HIP FILE
def get_current_render_asset_from_hip():
    hda_info_dict = create_hda_info_from_hip()
    target_nodetype = hda_info_dict.get('hda_name')
    network = hou.node('/obj/')
    type_matcher = nodesearch.NodeType(target_nodetype)
    found_hdas = type_matcher.nodes(network)

    if found_hdas:
        return found_hdas[0]
    else:
        return None


# GET RENDER ASSET WHICH IS READY TO BE EXPORTED NO MATTER IF IT'S RELATED TO THE CURRENT HIP FILE VERSION
def get_render_assets_to_be_exported(network='/obj/'):
    search_network  = hou.node(network)
    target_render_assets = None
    # SCAN ALL 'render_asset' PARMS OF ALL NODES IN SEARCH NETWORK for 'to_be_exported' VALUE AND RETURN THE NODE HOLDING THAT PARM
    target_render_assets = [i.node() for i in search_network.allParms() if i.name() == 'render_asset' and i.eval() == 'to_be_exported']

    return target_render_assets


# GET FOREIGN RENDER ASSETS (WHICH ARE NOT RELATED TO THE CURRENTLY OPENED FILE)
def get_foreign_render_assets():
    foreign_render_assets = []
    # GET ALL RENDER ASSETS TO BE EXPORTED
    all_export_render_assets = get_render_assets_to_be_exported()
    # GET RENDER ASSET THAT MATCHES EXACTLY THE CURRENT HIPFILE (ALSO VERSION WISE) AND EXCLUDE IT FROM THE LIST
    current_version_render_asset = get_current_render_asset_from_hip()
    other_render_assets = [i for i in all_export_render_assets if i is not current_version_render_asset]
    # CREATE THE PSEUDO INFO DICT FOR HOW THE HDA COMPONENTS OF CURRENT HIPFILE SHOULD LOOK LIKE
    # AND COMPARE THEM WITH THE FOUND COMPONENTS
    # IF ONLY VERSION IS MISMATCHING, IT'S NOT A FOREIGN RENDER ASSET BUT SIMPLY HAS A DIFFERENT VERSION THAN THE CURRENT HIPFILE
    current_hda_info_dict = create_hda_info_from_hip()
    current_namespace = current_hda_info_dict.get('hda_namespace')
    current_type_name = current_hda_info_dict.get('hda_type_name')

    # COMPARE
    for other_hda in other_render_assets:
        other_hda_file_path = get_filepath_from_hda(other_hda)
        other_definition = get_definition_from_hda_file(other_hda_file_path)
        other_hda_name = other_definition.nodeTypeName()
        other_namespace = other_hda_name.split('::')[0]
        other_type_name = other_hda_name.split('::')[1]

        # COMPARE NAMESPACE AND TYPE NAME
        if other_namespace != current_namespace or other_type_name != current_type_name:
            foreign_render_assets.append(other_hda)

    return foreign_render_assets


# GET HDA STATUS
def get_render_asset_status(hda_node=None, hda_file_path=None):
    status = None
    hda_definition = None

    if hda_file_path:
        hda_definition = get_definition_from_hda_file(hda_file_path)
    elif hda_node:
        hda_file_path = get_filepath_from_hda(hda_node)
        hda_definition = get_definition_from_hda_file(hda_file_path)

    if hda_definition:
        status = hda_definition.comment()

    return status


# UPDATE RENDER ASSET APPEARANCE
def update_render_asset_appearance(hda_node, hda_file_path, state):
    # SET HDA NODE COLOR TO STATE DEPENDENT COLOR
    set_node_color(hda_node, state)
    # SET HDA ICON
    hda_definition = get_definition_from_hda_file(hda_file_path)
    if state == 'unpublished':
        hda_definition.setIcon('box.svg')
    if state == 'published':
        hda_definition.setIcon('box_closed_arrow.svg')
    if state == 'published_but_not_current':
        hda_definition.setIcon('box_closed_arrow.svg')

    # SET HDA COMMENT
    comment = state.upper()
    # SET NODE COMMENT
    hda_node.setGenericFlag(hou.nodeFlag.DisplayComment, True)
    hda_node.setComment(comment)
    # UNLOCK HDA IF STATE IS NOT PUBLISHED
    if state != 'published':
        hda_node.allowEditingOfContents()


# CHANGE HDA DEFINITION STATUS
def change_hda_definition_status(status, hda_node=None, hda_file_path=None):
    status = status.upper()
    hda_definition = None

    if hda_file_path:
        hda_definition = get_definition_from_hda_file(hda_file_path)
    elif hda_node:
        hda_file_path = get_filepath_from_hda(hda_node)
        hda_definition = get_definition_from_hda_file(hda_file_path)

    if hda_definition:
        hda_definition.setComment(status)
    else:
        print(('COULD NOT FIND HDA DEFINITION FROM HDA:\n{}'.format(hda_file_path)))


# SET NODE COLOR BASED ON ITS STATE
def set_node_color(hda_node, state):
    # COLORS
    unpublished_color = (0.478, 0.478, 0.478)
    published_color = (0.363, 0.755, 0.054)
    published_but_not_current_color = (1.045, 0.478, 0.045)

    if state == 'unpublished':
        col = hou.Color(unpublished_color)
    if state == 'published':
        col = hou.Color(published_color)
    if state == 'published_but_not_current':
        col = hou.Color(published_but_not_current_color)

    hda_node.setColor(col)


# GET FILE PATH FROM HDA NODE
def get_filepath_from_hda(hda_node):
    file_path = hda_node.type().definition().libraryFilePath()
    return file_path


# SET FOCUS TO HDA NODE
def set_focus_to_hda_node(hda_node):
    hda_node.setSelected(True)
    p = hou.ui.paneTabOfType(hou.paneTabType.NetworkEditor)
    p.setCurrentNode(hda_node)
    p.homeToSelection()


# GET HDA DEFINITION FROM PATH
def get_definition_from_hda_file(hda_file_path):
    hda_definition = hou.hda.definitionsInFile(hda_file_path)[0]
    return hda_definition

import json
import spc_extract_dir_structure
import spc_global_pipeline_vars as spcvars
import importlib
importlib.reload(spc_extract_dir_structure)
importlib.reload(spcvars)


# READ THE PROJECT STRUCTURE DIR JSON FILE TO KNOW WHERE TO FIND THE PROJECT SETTINGS JSON FILE
def get_project_settings_json_file(project_root):
    proj_structure_json = spcvars.project_structure_json
    importlib.reload(spc_extract_dir_structure)
    settings_json = spc_extract_dir_structure.get_proj_settings_filepath(structure_json_file=proj_structure_json, root_dir=project_root)
    return settings_json


# RETURN ALL PROJECT SETTINGS AS A DICTIONARY
def get_all_from_project(project_root):
    settings_json = get_project_settings_json_file(project_root)
    settings_dict = {}
    try:
        with open(settings_json, 'r') as f:
            settings_dict = json.load(f)
    except:
        pass
    return settings_dict


# RETURN PROJECT SPECIFIC SETTING
def get_project_setting(project_root, setting):
    value = ''
    try:
        all_settings = get_all_from_project(project_root)
        value = all_settings.get(setting)
    except:
        pass
    return value


# READ THE TYPE STRUCTURE DIR JSON FILE TO KNOW WHERE TO FIND THE SHOT / ASSET / RND SETTINGS JSON FILE
def get_type_settings_json_file(type_dir):
    type_structure_json = spcvars.type_structure_json
    importlib.reload(spc_extract_dir_structure)
    settings_json = spc_extract_dir_structure.get_type_settings_filepath(structure_json_file=type_structure_json, type_dir=type_dir)
    return settings_json


# RETURN ALL SHOT / ASSET / RND SETTINGS AS A DICTIONARY
def get_all_from_type(type_dir):
    settings_json = get_type_settings_json_file(type_dir)
    settings_dict = {}
    try:
        with open(settings_json, 'r') as f:
            settings_dict = json.load(f)
    except:
        pass
    return settings_dict


# RETURN SHOT / ASSET / RND SPECIFIC SETTING
def get_type_setting(type_dir, setting):
    value = ''
    try:
        all_settings = get_all_from_type(type_dir)
        value = all_settings.get(setting)
    except:
        pass
    return value

# JSON FILES IN THIS PIPELINE ARE STRUCTURED LIKE THE FOLLOWING:
# The main dictionary key is always called "dirs"
# If a value is a dictionary itself, the key is the parent directory and the values are the sub directories.
# Those can again be lists (multiple subfolders directly below the parent directory) or dictionaries.
import json
import os
from pathlib import Path

helper_result_list = []


# GET THE JSON DATA
def get_json_data(structure_json_file):
    with open(structure_json_file, 'r') as f:
        json_data = json.load(f)
    return json_data


# RECURSIVELY UNPACK AND RETURN THE GLOBAL VARIABLE helper_result_list
def unpack_json_data(structure_json_file=None, root_dir=None, target_data_list=None, target_data='dirs'):
    global helper_result_list  # THIS LINE IS NEEDED TO MODIFY THE GLOBAL VARIABLE
    if not root_dir:
        root_dir = ''
    if structure_json_file:
        target_data_list = get_json_data(structure_json_file)[target_data]

    # MAIN LOOP
    for i in target_data_list:
        # UNICODE / STRING FOUND
        if isinstance(i, str):
            # COMPLETE PATH AND ADD TO RESULT LIST
            dir_to_add = os.path.join(root_dir, i)
            if not helper_result_list.count(dir_to_add):
                helper_result_list.append(dir_to_add)

        # LIST / TUPLE FOUND
        elif isinstance(i, (list, tuple)):
            for k in i:
                # LIST / TUPLE CAN EITHER CONTAIN A STRING / UNICODE OR A DICTIONARY
                # DICTIONARY FOUND
                if isinstance(k, dict):
                    sub_target_data_list = []
                    for key, value in list(k.items()):
                        sub_root_dir = os.path.join(root_dir, key)
                        sub_target_data_list.append(value)
                        # CALL FUNCTION AGAIN
                        unpack_json_data(root_dir=sub_root_dir, target_data_list=sub_target_data_list)
                # UNICODE / STRING FOUND
                elif isinstance(k, str):
                    # COMPLETE PATH AND ADD TO RESULT LIST
                    dir_to_add = os.path.join(root_dir, k)
                    if not helper_result_list.count(dir_to_add):
                        helper_result_list.append(dir_to_add)

        # DICTIONARY FOUND
        elif isinstance(i, dict):
            sub_target_data_list = []
            for key, value in list(i.items()):
                sub_root_dir = os.path.join(root_dir, key)
                sub_target_data_list.append(value)
                # CALL FUNCTION AGAIN
                unpack_json_data(root_dir=sub_root_dir, target_data_list=sub_target_data_list)
    # CLEAN DOUBLES
    helper_result_list = list(set(helper_result_list))
    return helper_result_list


# BUILD PATH FOR PROJECT SETTINGS JSON FILE
def get_proj_settings_filepath(structure_json_file, root_dir):
    dirs_to_scan = unpack_json_data(structure_json_file=structure_json_file, root_dir=root_dir)
    target_file_path = ''
    project_settings_folder = '_project_settings'
    for i in dirs_to_scan:
        if i.count(project_settings_folder):
            target_dir = os.path.join(i.rsplit(project_settings_folder)[0], i)
            target_file_path = os.path.join(target_dir, 'project_settings.json')
            break
    target_file_path = Path(target_file_path).as_posix()
    return target_file_path


# BUILD PATH FOR SHOT / ASSET / RND SETTINGS JSON FILE
def get_type_settings_filepath(structure_json_file, type_dir):
    dirs_to_scan = unpack_json_data(structure_json_file=structure_json_file, root_dir=type_dir)
    target_file_path = ''
    type_settings_folder = '_settings'
    for i in dirs_to_scan:
        if i.count(type_settings_folder):
            target_dir = os.path.join(i.rsplit(type_settings_folder)[0], i)
            target_file_path = os.path.join(target_dir, 'settings.json')
            break
    target_file_path = Path(target_file_path).as_posix()
    return target_file_path


# BUILD VARIOUS PATHS
def get_path_by_keyword(structure_json_file, root_dir, token):
    dirs_to_scan = unpack_json_data(structure_json_file=structure_json_file, root_dir=root_dir)
    target_path = ''
    for i in dirs_to_scan:
        if i.count(token):
            target_path = i
            break

    target_path = Path(target_path).as_posix()
    return target_path

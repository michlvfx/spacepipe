import spc_global_pipeline_vars as spcvars
import spc_extract_dir_structure
import os
import importlib
from pathlib import Path
importlib.reload(spcvars)
importlib.reload(spc_extract_dir_structure)

proj_structure_json = spcvars.project_structure_json
task_structure_json = spcvars.task_structure_json
type_structure_json = spcvars.type_structure_json

# GET SHOTS DIR
def get_shots_dir_main(project_root):
    shots_dir_main = spc_extract_dir_structure.get_path_by_keyword(proj_structure_json, project_root, 'shots')
    return shots_dir_main


# GET SHOT DIR
def get_shot_dir(project_root, shot_name):
    shots_dir_main = get_shots_dir_main(project_root)
    shot_dir = os.path.join(shots_dir_main, shot_name)
    shot_dir = Path(shot_dir).as_posix()
    return shot_dir


# GET ASSETS DIR MAIN
def get_assets_dir_main(project_root):
    assets_dir_main = spc_extract_dir_structure.get_path_by_keyword(proj_structure_json, project_root, 'assets')
    return assets_dir_main


# GET ASSET DIR
def get_asset_dir(project_root, asset_name):
    assets_dir_main = get_assets_dir_main(project_root)
    asset_dir = os.path.join(assets_dir_main, asset_name)
    asset_dir = Path(asset_dir).as_posix()
    return asset_dir


# GET RND DIR MAIN
def get_rnd_dir_main(project_root):
    rnd_dir_main = spc_extract_dir_structure.get_path_by_keyword(proj_structure_json, project_root, 'rnd')
    return rnd_dir_main


# GET RND DIR
def get_rnd_dir(project_root, rnd_name):
    rnd_dir_main = get_rnd_dir_main(project_root)
    rnd_dir = os.path.join(rnd_dir_main, rnd_name)
    rnd_dir = Path(rnd_dir).as_posix()
    return rnd_dir


# GET VARIOUS TASK DIR
def get_task_dir_by_keyword(task_dir, token):
    keyword_dir = spc_extract_dir_structure.get_path_by_keyword(task_structure_json, task_dir, token)
    return keyword_dir

# GET VARIOUS TYPE DIR
def get_type_dir_by_keyword(type_dir, token):
    keyword_dir = spc_extract_dir_structure.get_path_by_keyword(type_structure_json, type_dir, token)
    return keyword_dir

# GET VARIOUS PROJECT DIR
def get_project_dir_by_keyword(project_dir, token):
    keyword_dir = spc_extract_dir_structure.get_path_by_keyword(proj_structure_json, project_dir, token)
    return keyword_dir
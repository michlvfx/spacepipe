import sys
import os
import importlib

# CHECK IF CURRENT HOUDINI IS OPENED BY USER HYTHON (EG DEADLINE OR TOPS)
# IF VIA HYTHON, DONT TRY TO OPEN THE WINDOW, OTHERWISE IT'LL FAIL
if not 'hython' in os.path.basename(sys.executable):
    if not 'Hython' in os.path.basename(sys.executable):
        # OPEN THE MISSION CTRL WINDOW ON HOUDINI STARTUP
        try:
            import spc_mission_ctrl
            importlib.reload(spc_mission_ctrl)
            spc_mission_ctrl.window.show()
        except:
            print('SPACEPIPE: COULD NOT OPEN MISSION CTRL WINDOW')

        # ALLOW $JOB VARIABLE TO BE OVERRIDDEN
        import hou
        hou.allowEnvironmentToOverwriteVariable("JOB", True)
        hou.allowEnvironmentToOverwriteVariable("FPS", True)

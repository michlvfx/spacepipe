# SPACEPIPE

## Copy Folder:

Copy the complete **spacepipe-master** folder to your desired location and rename it to **spacepipe**.

## Set System Variables

The Spacepipe needs to have **2 global system variables** set to let Houdini initialize all the paths.
1. `SPACEPIPE_ROOT`
    1. This needs to point to the main directory where you placed the spacepipe folder. 
    2. Windows Example: *C:/_PIPELINE/spacepipe*
2. `HOUDINI_PACKAGE_DIR`
    1. This needs to point to the **_houEnv** folder inside the spacepipe directory
    2. Windows Example: *%SPACEPIPE_ROOT%/_houEnv*

You can find predefined startup files for Windows and MacOS under *../spacepipe/sourcefiles/..* that you can edit to your needs and place them inside your operating systems startup scripts folder.
1. Windows startup folder directory: *C:/ProgramData/Microsoft/Windows/Start Menu/Programs/StartUp*
2. Mac startup folder directory: *~/Library/LaunchAgents/*
3. Linux: *i/dont/know*

## Define Pipeline Directories

Go inside *../spacepipe/_houEnv* and edit the Houdini package files to your needs. Those are json files and define Houdini's environment.
To have the Spacepipe running, you just have to edit the **officeEnv.json** file and replace both the value for the **PROJECTROOT** and the **SPACEPIPE_TEMPDIR**.
1. Project Root: As the name suggests, this is the directory where your projects will be located at. 
2. Spacepipe Tempdir: The directory where some temporary files, created by the pipeline scripts, will be located at.

## Launch Houdini

Once you did the previous steps, you can fire up Houdini as usual. 
1. The Spacepipe's Mission CTRL Window should open automatically.
2. Activate the **Pipeline Shelf Set** by clicking on the little arrow on the upper right side of each shelf. You should now see the Spacepipes Shelf Set.







